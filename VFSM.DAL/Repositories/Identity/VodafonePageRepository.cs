﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Models;

namespace VFSM.DAL.Repositories
{
    public class VodafonePageRepository : Repository<VodafonePage>, IVodafonePageRepository
    {
        public VodafonePageRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _apContext => (ApplicationDbContext)_context;
    }
}
