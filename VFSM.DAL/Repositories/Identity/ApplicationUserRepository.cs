﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Globalization;
using VFSM.DAL.Models;
using VFSM.DAL.Repositories;

namespace VFSM.DAL.Repositories
{
    public class ApplicationUserRepository : Repository<ApplicationUser>, IApplicationUserRepository
    {
        public ApplicationUserRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _apContext => (ApplicationDbContext)_context;

        public dynamic GetUserByUserName(string username)
        {
            var query = (from user in _apContext.Users.Where(o => o.UserName == username)
                        join userRole in _apContext.UserRoles on user.Id equals userRole.UserId
                        select new
                        {
                            user.Id,
                            user.Email,
                            user.EmailConfirmed,
                            user.UserName,
                            user.PhoneNumber,
                            Roles = (
                                from rl in _apContext.Roles.Where(o => o.Id == userRole.RoleId)
                                select new 
                                {
                                    rl.Id, 
                                    rl.Name, 
                                    rl.Description,
                                    Pages = (
                                        from rp in _apContext.VodafonePageRoles.Where(o => o.RoleID == userRole.RoleId)
                                        join p in _apContext.VodafonePages on rp.PageID equals p.Id
                                        join o in _apContext.VodafoneOperations on rp.OperationID equals o.Id
                                        select new {
                                            rp.PageID,
                                            pageName = p.Name,
                                            pageValue = p.Value,
                                            rp.OperationID,
                                            operationValue = o.Value,
                                            operationName = o.Name,
                                        }
                                    )
                                }
                            ).FirstOrDefault()
                        }).FirstOrDefault();

                        //join role in
                        //(
                        //    from userRole in _apContext.UserRoles
                        //    join rl in _apContext.Roles on userRole.RoleId equals rl.Id

                        //    join menuRole in _apContext.VodafoneMenuRoles on rl.Id equals menuRole.RoleID
                        //    join menuOperation in
                        //    (
                        //        from _menuOperation in _apContext.VodafoneMenuOperations
                        //        join operation in _apContext.VodafoneOperations on _menuOperation.OperationID equals operation.Id
                                

                        //    )

                        //    select new ApplicationUserRole
                        //    {
                        //        UserId = userRole.UserId,
                        //        Role = rl
                        //        //Id = rl.Id,
                        //        //Name = rl.Name,
                        //        //ConcurrencyStamp = rl.ConcurrencyStamp,
                        //        //NormalizedName = rl.NormalizedName,
                        //        //UserRoles = userRole.UserId
                        //    }
                        //) on user.Id equals role.UserId

            return query;
        }

    }
}