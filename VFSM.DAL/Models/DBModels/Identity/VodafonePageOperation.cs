﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VFSM.DAL.Models
{
    [Table("T_ADMIN_PAGE_OPERATION")]
    public class VodafonePageOperation
    {
        [Key]
        [Required]
        [Column("PAGE_ID")]
        [StringLength(36)]
        public string PageID { get; set; }
        // public virtual VodafonePage VodafonePage { get; set; }

        [Key]
        [Required]
        [Column("OPERATION_ID")]
        [StringLength(36)]
        public string OperationID { get; set; }
        // public virtual VodafoneOperation VodafoneOperation { get; set; }
    }
}
