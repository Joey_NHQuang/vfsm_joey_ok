﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VFSM.DAL.Models
{
    public class VodafoneRole : ApplicationRole
    {          
        public string Description { get; set; }
    }
}
