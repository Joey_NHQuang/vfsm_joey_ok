using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VFSM.DAL.Models
{
    public class MenuItemModel
    {
        public int Id { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string MenuItemText { get; set; }
        public string Title { get; set; }
        public int ParentId { get; set; }
        public bool Disabled { get; set; } = false;

        public MenuItemModel()
        {
        }

        public MenuItemModel(int id, string menuItemText, string action, string controller, string title, int parentId, bool disabled = false)
        {
            Id = id;
            MenuItemText = menuItemText;
            ActionName = action;
            ControllerName = controller;
            Title = title;
            ParentId = parentId;
            Disabled = disabled;
        }
    }

    public class MenuItemListModel
    {
        public List<MenuItemModel> MenuItems { get; set; }

        public MenuItemListModel()
        {
            MenuItems = new List<MenuItemModel>();
        }

        public static MenuItemListModel GetEmptyMenus()
        {
            var MenuList = new MenuItemListModel();
            return MenuList;
        }

        public static MenuItemListModel GetMenus(IList<string> roles)
        {
            var MenuList = new MenuItemListModel();

            if (ControllerInRole.InRoles["CS"].Any(role => roles.Any(m => m == role)))
            {
                MenuList.MenuItems.Add(new MenuItemModel(10, "CS Operation", null, null, "CS Operation", 0));
                MenuList.MenuItems.Add(new MenuItemModel(100, "Customers & Devices", "Index", "User", "Customers & Devices", 10));
                MenuList.MenuItems.Add(new MenuItemModel(110, "Integration History", "Index", "AccountIntegrationHistory", "Integration History", 10));
                MenuList.MenuItems.Add(new MenuItemModel(120, "Beacon Location", "Index", "Beacon", "Beacon Location", 10));
            }

            if (ControllerInRole.InRoles["Service Managements"].Any(role => roles.Any(m => m == role)))
            {
                MenuList.MenuItems.Add(new MenuItemModel(20, "Service Managements", null, null, "Service Managements", 0));
                MenuList.MenuItems.Add(new MenuItemModel(200, "Models", "Index", "Model", "Models", 20));
                MenuList.MenuItems.Add(new MenuItemModel(210, "OTA", "Index", "OTA", "OTA", 20));
            }

            if (ControllerInRole.InRoles["Profile Manager"].Any(role => roles.Any(m => m == role)))
            {
                MenuList.MenuItems.Add(new MenuItemModel(40, "Admin Managements", null, null, "Admin Managements", 0));
                MenuList.MenuItems.Add(new MenuItemModel(400, "Profiles", "Index", "AdminUser", "Profiles", 40));
            }

            if (ControllerInRole.InRoles["Admin"].Any(role => roles.Any(m => m == role)))
            {
                MenuList.MenuItems.Add(new MenuItemModel(410, "Roles", "Index", "Role", "Roles", 40));
            }
            
            MenuList.MenuItems.Add(new MenuItemModel(999, "Contact", "Contact", "Home", "Contact", 0));

            return MenuList;
        }

    }

    public static class ControllerInRole
    {
        private static Dictionary<string, List<string>> inRoles;
        public static Dictionary<string, List<string>> InRoles
        {
            get
            {
                if (inRoles == null)
                {
                    inRoles = new Dictionary<string, List<string>>();
                    inRoles.Add("Admin", new List<string> { "Admin" });
                    inRoles.Add("Service Managements", new List<string> { "Admin", "Service Managements" });
                    inRoles.Add("CS", new List<string> { "Admin", "CS Read", "CS Write" });
                    inRoles.Add("Profile Manager", new List<string> { "Admin", "Profile Manager" });
                }
                return inRoles;
            }
        }
    }
}