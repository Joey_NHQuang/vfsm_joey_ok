﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace VFSM.DAL
{
    public class HttpUnitOfWork : UnitOfWork
    {
        public HttpUnitOfWork(VodafoneContext vfContext, ApplicationDbContext context, IHttpContextAccessor httpAccessor) : base(vfContext, context)
        {
            // Get username from Token. JwtRegisteredClaimNames.UniqueName: Reference to AccountController -> GetTokenClaims function
            var value = httpAccessor?.HttpContext?.User;
            context.CurrentUserName = value?.FindFirst(x => x.Type == ClaimTypes.Name)?.Value?.Trim();
        }
    }
}
