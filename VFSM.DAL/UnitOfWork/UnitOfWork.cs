﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Repositories;

namespace VFSM.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly VodafoneContext _vfContext;
        readonly ApplicationDbContext _context;

        IVodafoneUserRepository _vodafoneUser;
        IVodafoneDeviceRepository _vodafoneDevice;
        IApplicationUserRepository _user;
        IVodafoneBeaconsRepository _vodafoneBeacons;
        IVodafoneModelRepository _vodafoneModel;
        IVodafoneModelOTARepository _vodafoneModelOTA;
        IVodafoneUserDeviceRepository _vodafoneUserDevice;

        // Identity
        IApplicationRoleRepository _role;
        IVodafonePageRepository _vodafonePage;
        IVodafoneOperationRepository _vodafoneOperation;
        IVodafonePageRoleRepository _vodafonePageRole;
        IVodafonePageOperationRepository _vodafonePageOperation;


        public UnitOfWork(VodafoneContext vfContext, ApplicationDbContext context)
        {
            _vfContext = vfContext;
            _context = context;
        }


        public IVodafoneUserRepository VodafoneUsers
        {
            get
            {
                if (_vodafoneUser == null)
                    _vodafoneUser = new VodafoneUserRepository(_vfContext);
                return _vodafoneUser;
            }
        }

        public IVodafoneDeviceRepository VodafoneDevices
        {
            get
            {
                if (_vodafoneDevice == null)
                    _vodafoneDevice = new VodafoneDeviceRepository(_vfContext);
                return _vodafoneDevice;
            }
        }

        public IVodafoneBeaconsRepository VodafoneBeacons
        {
            get
            {
                if (_vodafoneBeacons == null)
                    _vodafoneBeacons = new VodafoneBeaconsRepository(_vfContext);
                return _vodafoneBeacons;
            }
        }

        public IVodafoneModelRepository VodafoneModel
        {
            get
            {
                if (_vodafoneModel == null)
                    _vodafoneModel = new VodafoneModelRepository(_vfContext);
                return _vodafoneModel;
            }
        }

        public IVodafoneModelOTARepository VodafoneModelOTA
        {
            get
            {
                if (_vodafoneModelOTA == null)
                    _vodafoneModelOTA = new VodafoneModelOTARepository(_vfContext);
                return _vodafoneModelOTA;
            }
        }

        public IVodafoneUserDeviceRepository VodafoneUserDevice
        {
            get
            {
                if (_vodafoneUserDevice == null)
                    _vodafoneUserDevice = new VodafoneUserDeviceRepository(_vfContext);
                return _vodafoneUserDevice;
            }
        }

        // Identity
        public IApplicationRoleRepository Role
        {
            get
            {
                if (_role == null)
                    _role = new ApplicationRoleRepository(_context);
                return _role;
            }
        }

        public IApplicationUserRepository Users
        {
            get
            {
                if (_user == null)
                    _user = new ApplicationUserRepository(_context);
                return _user;
            }
        }

        public IVodafonePageRepository VodafonePages
        {
            get
            {
                if (_vodafonePage == null)
                    _vodafonePage = new VodafonePageRepository(_context);
                return _vodafonePage;
            }
        }
        public IVodafoneOperationRepository VodafoneOperation
        {
            get
            {
                if (_vodafoneOperation == null)
                    _vodafoneOperation = new VodafoneOperationRepository(_context);
                return _vodafoneOperation;
            }
        }

        public IVodafonePageRoleRepository VodafonePageRole
        {
            get
            {
                if (_vodafonePageRole == null)
                    _vodafonePageRole = new VodafonePageRoleRepository(_context);
                return _vodafonePageRole;
            }
        }

        public IVodafonePageOperationRepository VodafonePageOperation
        {
            get
            {
                if (_vodafonePageOperation == null)
                    _vodafonePageOperation = new VodafonePageOperationRepository(_context);
                return _vodafonePageOperation;
            }
        }

        public int SaveChange()
        {
            return _context.SaveChanges();
        }
    }
}
