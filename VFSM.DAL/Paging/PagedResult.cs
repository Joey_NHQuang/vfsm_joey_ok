﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VinaHR.DAL
{
    public class PagedList<T> : PagingParams where T : class
    {
        public IEnumerable<T> Items { get; set; }
        public PagedList()
        {
            Items = new List<T>();
        }
    }

    public static class PagingList
    {
        public static PagedList<T> GetPage<T>(IQueryable<T> query, int page, int pageSize) where T : class
        {
            var result = new PagedList<T>();
            result.TotalItems = query.Count();

            if (page <= 0) page = 1;
            if (pageSize <= 0) pageSize = 10;

            result.CurrentPage = page;
            result.PageSize = pageSize;

            var pageCount = (double)result.TotalItems / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;

            if (query.Any())
                result.Items = query.Skip(skip).Take(pageSize);
            else
                result.Items = new List<T>();

            return result;
        }
    }
}
