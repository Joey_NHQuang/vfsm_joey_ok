﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using VFSM.API.Data;
using VFSM.API.Constants;
using VFSM.API.Models.AccountViewModels;
using VFSM.DAL.Models;
using VFSM.DAL.Models.DBModels;

namespace VFSM.API.Controllers
{
    
    public class HomeController : BaseController
    {
        public HomeController(VodafoneContext context, UserManager<ApplicationUser> userManager)
            : base(context, userManager)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
