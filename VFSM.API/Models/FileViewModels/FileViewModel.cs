using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.API.Models
{
    public class FileViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public long Size { get; set; }
        public string FileBase64 { get; set; }
    }
}