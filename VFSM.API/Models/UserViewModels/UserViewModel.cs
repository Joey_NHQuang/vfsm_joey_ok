using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.API.Models.UserViewModels
{
    public class UserViewModel
    {
        [Column("CREATED_DATE")]
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }

        [Column("USER_ID")]
        [Display(Name = "Customer ID")]
        public string UserId { get; set; }

        [Column("NICK_NAME")]
        [Display(Name = "Name")]
        public string NickName { get; set; }

        [Column("MOBILE_TYPE")]
        [Display(Name = "Mobile Type")]
        public string MobileType { get; set; }

        [Column("IMEI")]
        [Display(Name = "Imei")]
        public string Imei { get; set; }

        // [Column("DEVICE_COUNT")]
        // [Display(Name = "Device Count")]
        // public int DeviceCount { get; set; }
        public IEnumerable<DeviceOfUserViewModel> Device { get; set; }
    }
    public class UserShowViewModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }
        public string UserId { get; set; }
        public string NickName { get; set; }
        public string MobileType { get; set; }
        public string Imei { get; set; }
        public int DeviceCount { get; set; }
    }
    public class UserSearchViewModel : SortViewModels
    {
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd}")]
        public DateTime FromDate { get; set; }
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd}")]
        public DateTime ToDate { get; set; }
        public string UserId { get; set; }
        public string NickName { get; set; }
        public string MobileType { get; set; }
        public string Imei { get; set; }
        public string Mac { get; set; }
    }
    public class UserSearchForExportViewModel
    {
        public string UserId { get; set; }
        public string Imei { get; set; }
    }
    public class DeviceOfUserViewModel : DeviceResponseModel
    {
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}