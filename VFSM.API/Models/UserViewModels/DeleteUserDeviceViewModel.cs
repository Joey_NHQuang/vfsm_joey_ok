using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.API.Models.UserViewModels
{
    public class DeleteUserDeviceViewModel
    {
        public string DeviceId { get; set; }
        public string UserId { get; set; }
    }
}