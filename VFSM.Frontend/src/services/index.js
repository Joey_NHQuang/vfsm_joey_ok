import UserService from './UserService';
import DeviceService from './DeviceService';
import ModelService from './ModelService';
import OTAService from './OTAService';
import AuthenticationService from './AuthenticationService';
import EventService from './EventService';
import AccountService from './AccountService';
import RoleService from './RoleService';
import SeriesService from './SeriesService';

export {
    UserService,
    DeviceService,
    ModelService,
    OTAService,
    AuthenticationService,
    EventService,
    AccountService,
    RoleService,
    SeriesService
}