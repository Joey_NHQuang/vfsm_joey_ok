'use strict';
import { ApiService } from '../core/services';

class UserService extends ApiService {

	constructor() {
		super('user');
	}

	getAll(body) {
		return this.get('', body);
	}

	getDetail(userId) {
		return this.get(userId, {});
	}

	addNewDevice(dataBody) {
		return this.post('device', dataBody);
	}

	editNickName(dataBody) {
		return this.put('nick-name', dataBody);
	}

	exportUser(dataBody) {
		return this.get('export', dataBody);
	}

}

export default new UserService();