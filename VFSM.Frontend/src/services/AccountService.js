'use strict';
import { ApiService } from '../core/services';

class AccountService extends ApiService {

	constructor() {
		super('account');
    }
    
    signIn (payload) {
        return this.post('signin', payload);
	}

	ResetPassword (payload){
		return this.post('ResetPassword', payload);
	}

	forgotPassword(payload) {
		return this.post('ForgotPassword', payload);
	}
    
	getCurrentUser() {
		return this.get('current');
	}

	search(body) {
		return this.get('search', body);
	}

	getDetail(Id) {
		return this.get('Detail/'+Id, {});
	}

	addNew(dataBody) {
		return this.post('create', dataBody);
    }
    
	edit(dataBody) {
		return this.put('edit/'+dataBody.id, dataBody);
    }
    
	remove(id) {
		return this.delete('remove/'+id);
    }
    

}

export default new AccountService();