import { observable, action, configure, runInAction } from 'mobx';
import { DialogService } from 'core/services';

import Dashboard from 'components/Dashboard/Dashboard';
import Monitoring from 'components/Monitoring/Monitoring';
import DeviceEvent from 'components/DeviceEvent/DeviceEvent';
import Maintenance from 'components/Maintenance/Maintenance';
import Report from 'components/Report/Report';
import Account from 'components/Admin/Account/Account';
import Role from 'components/Admin/Role/Role';
import TesterPage from 'components/TesterPage/TesterPage';

// don't allow state modifications outside actions
configure({
	enforceActions: 'observed'
});
class MenuRoleStore {

    @observable menuRoles = {
        // dashboard : '',
        monitoring : '',
        deviceEvent : '',
        maintenance : '',
        report : '',
        admin : '',
        user : '',
        rolePolicy : '',
        tester : ''
    };
    @observable routes = [
        // {
        //     pageValue: 'page.dashboard',
        //     operationValue: '',
        //     name: 'Dashboard',
        //     icon: 'Dashboard',
        //     path: '/main/Dashboard',
        //     component: Dashboard,
        //     exact: true
        // },
        {
            pageValue: 'page.monitoring',
            operationValue: '',
            name: 'Monitoring',
            icon: 'Monitoring',
            path: '/main/Monitoring',
            component: Monitoring,
            exact: false
        },
        {
            pageValue: 'page.device.event',
            operationValue: '',
            name: 'Device Event',
            icon: 'DeviceEvent',
            path: '/main/DeviceEvent',
            component: DeviceEvent,
            exact: false
        },
        {
            pageValue: 'page.maintenance',
            operationValue: '',
            name: 'Maintenance',
            icon: 'Maintenance',
            path: '/main/Maintenance',
            component: Maintenance,
            exact: false
        },
        // {
        //     pageValue: 'page.report',
        //     operationValue: '',
        //     name: 'Report',
        //     icon: 'Report',
        //     path: '/main/Report',
        //     component: Report,
        //     exact: false
        // },
        {
            pageValue: 'page.admin',
            operationValue: '',
            name: 'Admin',
            icon: 'Admin',
            exact: false,
            children: [
                {
                    pageValue: 'page.user',
                    operationValue: '',
                    name: 'User',
                    path: '/main/admin/User',
                    component: Account
                },
                {
                    pageValue: 'page.role.policy',
                    operationValue: '',
                    name: 'Role & Policy',
                    path: '/main/admin/Role',
                    component: Role
                },
                // {
                //     pageValue: 'page.tester',
                //     operationValue: '',
                //     name: 'Tester page',
                //     path: '/main/admin/TesterPage',
                //     component: TesterPage
                // }
            ]
        }
    ];

    @action
    setMenuRoles(key, value) {
        switch (key) {
            // case 'page.dashboard':
            //     this.menuRoles.dashboard = value;
            //     this.routes[0].operationValue = value;
            //     break;
            case 'page.monitoring':
                this.menuRoles.monitoring = value;
                this.routes[0].operationValue = value;
                break;
            case 'page.device.event':
                this.menuRoles.deviceEvent = value;
                this.routes[1].operationValue = value;
                break;
            case 'page.maintenance':
                this.menuRoles.maintenance = value;
                this.routes[2].operationValue = value;
                break;
            // case 'page.report':
            //     this.menuRoles.report = value;
            //     this.routes[3].operationValue = value;
            //     break;
            // case 'page.admin':
            //     this.menuRoles.admin = value;
            //     this.routes[3].operationValue = value;
            //     break;
            case 'page.user':
                this.menuRoles.admin = value;
                this.routes[3].operationValue = value;
                this.menuRoles.user = value;
                this.routes[3].children[0].operationValue = value;
                break;
            case 'page.role.policy':
                this.menuRoles.admin = value;
                this.routes[3].operationValue = value;
                this.menuRoles.rolePolicy = value;
                this.routes[3].children[1].operationValue = value;
                break;
            // case 'page.tester':
            //     this.menuRoles.tester = value;
            //     this.routes[3].children[2].operationValue = value;
            //     break;
        
            default:
                break;
        }
    }

}

export default new MenuRoleStore();