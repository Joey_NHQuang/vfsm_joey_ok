import React, { Component } from 'react';
import ReactSVG from 'react-svg'

class Icon extends Component {
    render() {
        const { type, className, ...otherProps } = this.props;
        let _className = className || '';
        return (
            <span className={"icon-" + type + ' ' + _className}  {...otherProps}/>

        );
    }

};

export default Icon;
