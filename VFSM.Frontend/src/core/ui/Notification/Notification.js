import { notification } from 'antd';
import './styles.scss';

const onCloseNotification = (onCloseCallBack) => {
    if(onCloseCallBack){
        onCloseCallBack();
    }
}

const onOpenNotification = (customMsg, msg) => {
    notification.open({
        message: '',
        description: customMsg ? customMsg : '',
        placement: 'bottomRight',
        className: msg.toUpperCase() === 'SUCCESS' ? 'vfsm-notification' : 'vfsm-notification-error',
        onClose: () => onCloseNotification(),
    });
}

export default {
    onOpenNotification,
}