
import React, { Component } from 'react';
import NodeName from './NodeName';
//styles
import './style.scss';

export default class Node extends Component {

    render() {
        const { data, renderNodeName, level, index, siblings } = this.props;
        const { children } = data;

        const hasChildren = children && children.length;

        const length = hasChildren ? children.length : 0;

        const _children = hasChildren ?
            children.map((child, index) => {
                const first = index === 0;
                const last = index === length - 1;

                return (
                    <Node data={child} level={level + 1} index={index} siblings={length} renderNodeName={renderNodeName} key={index} />
                )
            })
            : null;

        let firstLast = '';
        if (siblings > 1) {
            if (index === siblings - 1) {
                firstLast = ' last-child';
            } else if (index === 0) {
                firstLast = ' first-child';
            } else {
                firstLast = ' mid-child';
            }
        }

        return (
            <div className={'node level-' + level + firstLast}>
                <div className={'node-name-wrap' + (level === 0 ? ' is-root' : '') + (hasChildren ? ' has-children' : '')}>
                    <NodeName data={data} level={level} renderNodeName={renderNodeName} />
                </div>
                <div className='node-children'>
                    {_children}
                </div>
            </div>
        )
    }
}
