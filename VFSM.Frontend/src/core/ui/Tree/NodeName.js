
import React, { Component } from 'react';
import { DragSource, DropTarget } from 'react-dnd';

const Types = {
    Node: 'node'
};

const nodeSource = {
    beginDrag(props) {
        // passes in the data
        const data = { props };
        return data;
    },

    endDrag(props, monitor, component) {
        if (!monitor.didDrop()) {
            return;
        }

        // When dropped, do something
        const data = monitor.getItem();
        const dropResult = monitor.getDropResult();
        console.log(dropResult);
    }
};

const nodeTarget = {
    drop(props) {
        console.log('dropped');
    }
};

@DragSource(Types.Node, nodeSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
@DropTarget(Types.Node, nodeTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    test: nodeTarget
}))
export default class NodeName extends Component {

    render() {
        const { isOver, isDragging, connectDropTarget, connectDragSource, data, renderNodeName, level } = this.props;
        const hasChildren = data.children && data.children.length > 0;
        const className = 'node-name' + (isOver ? ' active' : '')
            + (isDragging ? ' dragging' : '')
            + (hasChildren ? ' has-children' : '')
            + (level === 0 ? ' is-root' : '');

        const content = renderNodeName ? renderNodeName(data) : data.name;
        return (
            connectDragSource &&
            connectDropTarget &&
            connectDragSource(connectDropTarget(
                <span className={className}>{content}</span>
            ))
        )

    }
}
