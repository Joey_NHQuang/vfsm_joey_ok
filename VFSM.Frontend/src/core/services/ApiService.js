'use strict';
import Cookies from 'js-cookie';
import 'whatwg-fetch';
import { inject, observer } from 'mobx-react';
import CONFIG from './config';

class ApiService {
	constructor(url = '', headers = {}) {
        this.headers = headers;

        const http = CONFIG.ssl ? 'https' : 'http';
        this.url = `${http}://${CONFIG.domain}:${CONFIG.port}/${url}`
		
    }
    
	callApi = (METHOD, url = '', body, headers = {}) =>{
		const token = Cookies.get("token");
		const _headers = Object.assign({}, this.headers, {'Content-Type': 'application/json', Authorization: token}, headers);
		const options = {
			method: METHOD,
			headers: new Headers(_headers),
			mode: 'cors'
        };
        
		let queryString = '';
		if(METHOD === 'GET') {
			queryString = this.buildQueryString(body);
		} else {
			if(body) {
				options.body = JSON.stringify(body);
			}
        }

        if(url){
            url = '/' + url;
        }
        
        const _url = this.url + url + queryString;
        const request = new Request(_url, options);
        return fetch(request)
            .then(this.checkStatus)
            .then(this.parseJSON)
            .then(
                response => ({
                    response
                }),
                error => {
                    return {
                        error
                    }
                }
            );
    }
    
    parseJSON = (response) => {
        if (response.status === 204 || response.status === 205) {
            return null;
        }
        return response.json();
    }
    
    checkStatus = (response) => {
        if ((response.status >= 200 && response.status < 300) || response.status === 404 || response.status === 409) {
            return response;
        }
        // Check Unauthorized redirect to login
        if (response.status === 401 && response.statusText === 'Unauthorized') {
            window.location.href = '/login';
            Cookies.remove('token');
        }
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
    }

    buildQueryString = (body)=> {
        let query = '';
        if(body) {
            Object.keys(body).forEach(key => {
                if (key === 'undefined' || body[key] === undefined || body[key] === '' || body[key] === null) {
                    delete body[key];
                } else {
                    if(query === '') {
                        query = '?';
                    } else {
                        query += '&';
                    }
                    query += key + '=' + body[key];
                }
            });
        }

        return query;
    }

	get = (url, body, headers={}) =>{
		return this.callApi('GET', url, body, headers);
	}

	post = (url, body, headers={}) => {
		return this.callApi('POST', url, body, headers);
	}

	put = (url, body, headers={}) => {
		return this.callApi('PUT', url, body, headers);
	}

	patch = (url, body, headers={}) => {
		return this.callApi('PATCH', url, body, headers);
	}

	delete = (url, body, headers={}) => {
		return this.callApi('DELETE', url, body, headers);
	}
}

export default ApiService;