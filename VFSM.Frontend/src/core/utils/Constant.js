const USER_ERROR = {
    EMPTY: '',
    EXISTED: 'This Email is existed',
    INVALID: 'Incorrect Email Address',
    REQUIRED: 'This field is required',
    INCORRECT_FORMAT: 'Incorrect Email address format',
    NOT_FOUND: 'User not found',
    NOT_SAME: 'Confirm Email not same'
}

const API_STATUS = {
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR',
    EXITED: 'EXITED'
}

const PASSWORD_ERROR = {
    EMPTY: '',
    MIN_LENGTH: 'Passwords must be at least 6 characters',
    ONE_LOWER_CASE: 'Passwords must have at least one lowercase (a-z)',
    ONE_UPPER_CASE: 'Passwords must have at least one uppercase (A-Z)',
    ONE_DIGIT: 'Passwords must have at least one digit (0-9)',
    ONE_ALPHANUMERIC: 'Passwords must have at least one non alphanumeric character',
    NOT_SAME: 'Comfirm password not same',
    REQUIRED: 'This field is required',
    INVALID: 'Incorrect Password'
}

const EMAIL_GUIDMSG = {
    EMPTY: '',
    CONFIRM_SUSSESS : 'Reset link sent to your email',
}

export default { 
    USER_ERROR,
    PASSWORD_ERROR,
    EMAIL_GUIDMSG,
    API_STATUS
}