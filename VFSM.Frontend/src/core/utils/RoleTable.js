import React from 'react';
import { Switch } from 'antd';

const role = {
    Read: '22784ce6-2222-42c2-af5c-2536667d3333',
    Write: '22784ce6-2222-42c2-af5c-2536667d4444',
    Hide: '',
}

const page = [
//     {
//     Id: '22784ce6-2222-42c2-af5c-2536667d8991', 
//     Name: 'Dashboard', 
//     Value: 'page.dashboard', 
//     Order: 1, 
//     Parent_ID: null
// },
{
    Id: '22784ce6-2222-42c2-af5c-2536667d8992', 
    Name: 'Monitoring', 
    Value: 'page.monitoring', 
    Order: 2, 
    Parent_ID: null
},
{
    Id: '22784ce6-2222-42c2-af5c-2536667d8993', 
    Name: 'Device Event', 
    Value: 'page.device.event', 
    Order: 3, 
    Parent_ID: null
},
{
    Id: '22784ce6-2222-42c2-af5c-2536667d8994', 
    Name: 'Maintenance', 
    Value: 'page.maintenance', 
    Order: 4, 
    Parent_ID: null
},
// {
//     Id: '22784ce6-2222-42c2-af5c-2536667d8995', 
//     Name: 'Report', 
//     Value: 'page.report', 
//     Order: 5, 
//     Parent_ID: null
// },
{
    Id: '22784ce6-2222-42c2-af5c-2536667d8996', 
    Name: 'Admin', 
    Value: 'page.admin', 
    Order: 1, 
    Parent_ID: null
},
{
    Id: '22784ce6-2222-42c2-af5c-2536667d8997', 
    Name: 'User', 
    Value: 'page.user', 
    Order: 1, 
    Parent_ID: '22784ce6-3333-42c2-af5c-2536667d8996'
},
{
    Id: '22784ce6-2222-42c2-af5c-2536667d8998', 
    Name: 'Role & Policy', 
    Value: 'page.role.policy', 
    Order: 2, 
    Parent_ID: '22784ce6-3333-42c2-af5c-2536667d8996'
},
// {
//     Id: '22784ce6-2222-42c2-af5c-2536667d8999', 
//     Name: 'Role & Policy', 
//     Value: 'page.role.policy', 
//     Order: 2, 
//     Parent_ID: '22784ce6-3333-42c2-af5c-2536667d8996'
// }
];

export default {
    role ,
    page
}
