import React from 'react';
import {Col, Row } from 'antd';
import './styles.scss'

class ColModal extends React.Component{

    render(){

        const { data } = this.props;

        return(
            <div>
                    <Row className='customer-detail-row' style={{ borderStyle: 'solid' }}>
                        {
                            data.children ?  (
                                <div>
                                    <Col span={6} className="customer-chil-detail-col-name">
                                        <span className="customer-detail-col-name-span"> {data.title} </span>
                                    </Col>
                                    <Col span={18} className="add-col-chil-detail">
                                    <Row>
                                        { data.children.map((prop, index) => 
                                            <div>
                                                <Col span={6} className="customer-detail-col-name">
                                                    <span className="customer-detail-col-name-span"> {prop.title} </span>
                                                </Col>
                                                {
                                                    !prop.hasInput
                                                    ?  <Col span={18} style={{ 'paddingLeft': '52px', 'paddingRight': '10px', 'display': 'flex', 'alignItems': 'center' , 'paddingTop' : '0px'}}
                                                            className="customer-detail-col-detail"
                                                        >
                                                        <span className="customer-detail-col-detail-span"> {prop.content} </span>
                                                    </Col>
                                                    : <Col span={18} className="add-col-detail">
                                                        <span className="add-col-detail-span"> {prop.content} </span>
                                                    </Col>
                                                }
                                            </div>
                                        )}
                                        </Row>
                                    </Col>
                                </div>
                            ) : (
                                <div>
                                    <Col span={6} className="customer-detail-col-name">
                                        <span className="customer-detail-col-name-span"> {data.title} </span>
                                    </Col>
                                    {
                                        !data.hasInput
                                        ?  <Col span={18} style={{ 'paddingLeft': '52px', 'paddingRight': '10px', 'display': 'flex', 'alignItems': 'center' , 'paddingTop' : '0px'}}
                                                className="customer-detail-col-detail"
                                            >
                                            <span className="customer-detail-col-detail-span"> {data.content} </span>
                                        </Col>
                                        : <Col span={18} className="add-col-detail">
                                            <span className="add-col-detail-span"> {data.content} </span>
                                        </Col>
                                    }
                                </div>
                            )
                        }
                    
                    </Row>    
            </div>
        )
    }
}
export default ColModal;