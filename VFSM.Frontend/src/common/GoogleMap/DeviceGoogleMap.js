import React from 'react';
import PropTypes from 'prop-types';
import { Map, Marker, GoogleApiWrapper, InfoWindow } from 'google-maps-react';
import { Row } from 'antd';
import  GoogleAPIKey  from  '../../../src/core/services/GoogleAPIKey'; 
import ReactDOMServer from 'react-dom/server';
import  stringUtils  from '../../core//utils/String'
import './styles.scss';

const { convertMac } = stringUtils;
const API_KEY = GoogleAPIKey.KEY;

export class DeviceGoogleMap extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            titleMatch: '',
            markerActive: {},
            isShowInfoWindow: true,
        }
        this.makerOnClick = this.makerOnClick.bind(this);
        this.onDoubleClick = this.onDoubleClick.bind(this);

    }


    componentDidMount() {
        const bounds = new window.google.maps.LatLngBounds();
        const { data } = this.props;
        if (data.length > 1) { 
            if(this.checkLocation(data)){
                this.setState({
                    titleMatch: 'Device + Beacon'
                })
            }
        }
        this.props.data.map((result) => {
            return bounds.extend(new window.google.maps.LatLng(result.location));
        });
        //  set 
        if(this.props.data.length <= 1 && this.refs.resultMap){
            this.refs.resultMap.map.setCenter(this.props.data[0].location);
            this.refs.resultMap.map.setZoom(3);
        } else if (this.refs.resultMap){
            this.refs.resultMap.map.fitBounds(bounds);
        }
        //this.refs.resultMap.map.backgroundColor("gray")
    }

    onMapClicked = () => {
        const { isShowInfoWindow } = this.state;
        if(isShowInfoWindow){
            this.setState({
                markerActive: null,
            })
        }
        this.inforWindowClose();
    }

    inforWindowClose = () => {
        this.setState({
           // markerActive: null,
            isShowInfoWindow: false,
        });
    }

    makerOnClick = ( props, marker, e) => {
     //   props.preventDefault();
        const { isShowInfoWindow } = this.state;
        this.setState({
            markerActive: marker,
            isShowInfoWindow: !isShowInfoWindow,
        });
    }

    onDoubleClick() {
        const bounds = new window.google.maps.LatLngBounds()
        this.props.data.map((result) => {
            return bounds.extend(new window.google.maps.LatLng(result.location));
        });
        //  set    
        if(this.props.data.length <= 1 && this.refs.resultMap){
            this.refs.resultMap.map.setCenter(this.props.data[0].location);
            this.refs.resultMap.map.setZoom(3);
        } else if (this.refs.resultMap){
            this.refs.resultMap.map.fitBounds(bounds);
        }
    }

    checkLocation = (arr) => {
        const temp = arr[0].location;
        if(arr.length > 1){
            for( let i = 1 ; i< arr.length;i++){
                if(temp.lat === arr[i].location.lat && temp.lng === arr[i].location.lng){
                    return true;
                }
            }
        }
        return false;
    }

    render() {
        const { data } = this.props;
        const { markerActive, isShowInfoWindow, titleMatch } = this.state;
        var bounds = new this.props.google.maps.LatLngBounds();
        for (var i = 0; i < data.length; i++) {
            bounds.extend(data[i].location) ;
        }
        //this.props.google.maps.Map.fitBounds(bounds);
        return (
            (data && data.length > 0) &&
            <Row style={{ height: '20vh', width: '100%' }}>
                <Map
                    className= {data.length === 1 ? "vfsm-google-map-1" : "vfsm-google-map-2" } 
                    google={this.props.google}
                    ref="resultMap"
                    minZoom={2}
                    initialCenter={data[0].location}
                    disableDefaultUI={true}
                    fitBounds={bounds}
                    mapTypeId={'roadmap'}
                    backgroundColor={'gray'}
                    onClick={this.onMapClicked}
                >
                    {
                        data && data.map((prop, index) =>
                            (prop.location.lat !== undefined  && prop.location.lng !== undefined 
                            && prop.location.lat !== null  && prop.location.lng !== null) &&
                            <Marker
                                className="vfsm-google-map-marker"
                                title={titleMatch !== '' ? titleMatch : prop.title}
                                label={titleMatch !== '' ? titleMatch : prop.title}
                               // label={ReactDOMServer.renderToStaticMarkup(<div className='vfsm-google-map-title'>{prop.title}</div>)} 
                                name={prop.title}
                                onClick={this.makerOnClick}
                                onDblclick={e => { this.onDoubleClick(e) }}
                                position={prop.location}
                                icon={'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png'}
                                inforMac={prop.beaconMac ? prop.beaconMac : undefined }
                                key={index}
                            >
                                {
                                    prop.beaconMac && <div>{convertMac(prop.beaconMac)}</div>
                                }
                            </Marker>

                        )
                    }
                    { 
                        markerActive !== null &&
                        <InfoWindow 
                            marker={ markerActive}
                            onClose={this.inforWindowClose}
                            visible={ isShowInfoWindow && markerActive.inforMac !== undefined }
                        >
                            <div className="vfsm-google-maps-info-box"> 
                                <h4>{ markerActive.inforMac ? convertMac(markerActive.inforMac) : ''}</h4>
                            </div>
                        </InfoWindow>
                    }
                </Map>
            </Row>
        )
    }
}

DeviceGoogleMap.propTypes = {
    data: PropTypes.array.isRequired,
}

export default GoogleApiWrapper({
    apiKey: (API_KEY)
})(DeviceGoogleMap)