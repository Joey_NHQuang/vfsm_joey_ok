import React from 'react';
import { Menu, Dropdown } from 'antd';


import IconSvg from '../icon/IconSvg';
import './styles.scss'

export default class SmallSelect extends React.Component {


    handleClickMenu = (eventName) => {
        this.props.handleClickMenuCallBack(eventName);
    }

    menu = () => {
        const {selectOption } = this.props;
        return <Menu style={{ width: 130 }}>
            {
                selectOption && selectOption.map((prop, index) =>
                    <Menu.Item key={index} className={prop.className ? prop.className : ''} disabled={prop.disabled ? prop.disabled : false}>
                        {
                            prop.disabled
                                ? <span>{prop.title ? prop.title : ''}</span>
                                : <div className={prop.className ? prop.className : ''} onClick={() => this.handleClickMenu(prop.title)}>
                                    {prop.title ? prop.title : ''}
                                </div>
                        }
                    </Menu.Item>
                )
            }
        </Menu>
    };

    render() {

        return (
            <div className="small-select-wapper">
                <Dropdown overlay={this.menu()} trigger={['click']} >
                    <IconSvg name="More" className="model-card-icon" />
                </Dropdown>
            </div>
        )
    }
}
