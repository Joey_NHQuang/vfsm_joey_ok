import React, { Component } from 'react';
import { observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import Cookies from 'js-cookie';
import { Route, Switch } from 'react-router-dom';

//3rd party
import PerfectScrollbar from 'react-perfect-scrollbar';

import SideBar from './common/SideBar/SideBar'
import Header from './common/Header/Header'
import { APP_VERSION } from 'core/services/config';
//styles

import './styles/App.scss';
// import routes from './routes'
@inject('rootStore')
@inject('menuRoleStore')
@observer
class App extends Component {
    @observable token = undefined;

    constructor(props) {
        super(props);
        this.token = Cookies.get('token');
        this.state = {
            locationName: ''
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.location.pathname !== prevState.locationName) {
            return {
                locationName: nextProps.location.pathname,
            };
        }

        return null;
    }


    // Check token exist
    componentDidMount() {
        document.title = APP_VERSION;
        // Get current user login
        this.props.rootStore.account.getCurrentUser();
        //
        if (window.location.pathname === '/' || window.location.pathname === '/main') {
            this.props.history.replace('/main/Monitoring');
        }
        //
        if (this.token && this.token.length > 0) {
            if (window.location.href === window.location.origin
                || window.location.href === `${window.location.origin}/`) {
                this.props.history.replace('/main/Monitoring');
            }
            // } else {
            //   this.props.history.replace({pathname: '/main', query: {return_to: window.location.href}});
            // }
        } else {
            this.props.history.replace('/login');
        }
    }

    render() {
        const { locationName } = this.state;
        const flatRoutes = [];
        const { routes } = this.props.menuRoleStore;
        routes.forEach(group => {
            if (group.operationValue !== '' && group.operationValue !== null) {
                if (group.children) {
                    group.children.forEach(item => { flatRoutes.push(item); })
                } else {
                    flatRoutes.push(group);
                }
            }
        })

        return (
            <div className="app">
                <SideBar />
                <div className="main">
                    <Header locationName={locationName} />
                    <PerfectScrollbar className="page-content">
                        <Switch>
                            {
                                flatRoutes.map((route, index) => {
                                    return <Route exact={route.exact} path={`${route.path}`} component={route.component} key={index} />;
                                })
                            }
                            {/* <Redirect to={browse} /> */}
                        </Switch>
                    </PerfectScrollbar>
                </div>
                {/* <h1>Hi</h1>
                <img src="images/a.png" className="App-logo" alt="logo" /> */}
            </div>
        );
    }
}

export default App;
