// import Dashboard from './components/Dashboard/Dashboard';
import Monitoring from './components/Monitoring/Monitoring';
import DeviceEvent from './components/DeviceEvent/DeviceEvent';
import Maintenance from './components/Maintenance/Maintenance';
import Report from './components/Report/Report';
import Account from './components/Admin/Account/Account';
import Role from './components/Admin/Role/Role';
import TesterPage from './components/TesterPage/TesterPage';

const routes = [
    // {
    //     pageValue: 'page.dashboard',
    //     operationValue: 'operation.read',
    //     name: 'Dashboard',
    //     icon: 'Dashboard',
    //     path: '/main/Dashboard',
    //     component: Dashboard,
    //     exact: true
    // },
    {
        pageValue: 'page.monitoring',
        operationValue: 'operation.read',
        name: 'Monitoring',
        icon: 'Monitoring',
        path: '/main/Monitoring',
        component: Monitoring,
        exact: false
    },
    {
        pageValue: 'page.device.event',
        operationValue: 'operation.read',
        name: 'Device Event',
        icon: 'DeviceEvent',
        path: '/main/DeviceEvent',
        component: DeviceEvent,
        exact: false
    },
    {
        pageValue: 'page.maintenance',
        operationValue: 'operation.read',
        name: 'Maintenance',
        icon: 'Maintenance',
        path: '/main/Maintenance',
        component: Maintenance,
        exact: false,
        title: 'Model',
        childrenPage: [
            {
                name : 'OTA',
                title: 'OTA',
                path: '/main/Maintenance/OTA'
            }
        ]
    },
    // {
    //     pageValue: 'page.report',
    //     operationValue: 'operation.read',
    //     name: 'Report',
    //     icon: 'Report',
    //     path: '/main/Report',
    //     component: Report,
    //     exact: false
    // },
    {
        pageValue: 'page.admin',
        operationValue: 'operation.read',
        name: 'Admin',
        icon: 'Admin',
        exact: false,
        children: [
            {
                pageValue: 'page.user',
                operationValue: 'operation.read',
                name: 'User',
                path: '/main/admin/User',
                component: Account
            },
            {
                pageValue: 'page.role.policy',
                operationValue: 'operation.read',
                name: 'Role & Policy',
                path: '/main/admin/Role',
                component: Role
            },
            // {
            //     pageValue: 'page.tester',
            //     operationValue: 'operation.read',
            //     name: 'Tester page',
            //     path: '/main/admin/TesterPage',
            //     component: TesterPage
            // }
        ]
    }
];

export default routes;
