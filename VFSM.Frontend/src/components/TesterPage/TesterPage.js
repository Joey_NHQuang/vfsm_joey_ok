import React from 'react';
import { inject, observer } from 'mobx-react';
// import PropTypes from 'prop-types';

@inject('rootStore')
@observer
class TesterPage extends React.Component {


    render() {
        return (

            <div>
                This is Tester Page page
            </div>
        );
    }
}

// TesterPage.propTypes = {

// };
export default TesterPage;