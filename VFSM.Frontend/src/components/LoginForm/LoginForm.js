import React from 'react';
import { inject, observer } from 'mobx-react';
import Cookies from 'js-cookie';
import { Form, Icon, Input, Button, Checkbox, Row, Col } from 'antd';
import  VodaButton  from '../../common/VodaButton/VodaButton';
import VodaInput from '../../common/Input/VodaInput';
import VodaCheckBox from '../../common/Input/VodaCheckBox';
import WelcomeHeader from '../../common/WelcomeHeader/WelcomeHeader';
import { validatePWLowerCase , validatePWUpperCase, validatePWAlpha , validatePWMinLen, validatePWDigit} from '../../core/utils/validate'
import Constant  from '../../../src/core/utils/Constant';
// import css
import './styles.scss';
const FormItem = Form.Item;
const { PASSWORD_ERROR, USER_ERROR } = Constant;
@inject('rootStore')
@observer
class LoginForm extends React.Component{

    constructor(props){
        super(props);
        this.token = Cookies.get('token');
        this.forgotPwdUrl = `/password/requestReset`;
        
        this.state={
            firstVisit: false,
            remember: true,
            isShowPassWord : false,
            emailInput: '',
            passwordInput: '',
            userNameAuthStatus: '',
            passwordAuthStatus: '',
            isShowErrorEmail: false,
            isShowErrorPassword: false,
            errorEmailMsg: '',
            errorPassordMsg: '',
        }
    }

    componentDidMount(){
        // if (this.token && this.token.length > 0) {
        //     if (window.location.href === window.location.origin
        //         || window.location.href === `${window.location.origin}/`) {
        //         this.props.history.replace('/main');
        //     } else {
        //         this.props.history.replace({pathname: '/main', query: {return_to: window.location.href}});
        //     }
        // } else {
        //     this.props.history.replace('/login');
        // }
    }

    /* Call Back*/
    loginCallBack = (msg) => {
        if ( typeof msg === 'string' && msg.toUpperCase() === 'SUCCESS'){
            this.setState({
                isShowErrorPassword: false,
                isShowErrorEmail: false,
                errorPassordMsg: "",
                errorPassordMsg: "",
                emailInput: '',
                passwordInput: '',
            })
            window.location.replace('/main');
        } 
        else if (msg === 'UserInvalid'){
            this.setState({
                isShowErrorEmail: true,
                errorEmailMsg: USER_ERROR.INVALID,
            })
        }
        else if (msg === 'PassInvalid'){
            this.setState({
                isShowErrorPassword: true,
                errorPassordMsg: PASSWORD_ERROR.INVALID,
            })
        }
        else  {
            this.setState({
                isShowErrorPassword: true,
                isShowErrorEmail: true,
                errorEmailMsg: USER_ERROR.INVALID,
                errorPassordMsg: PASSWORD_ERROR.INVALID,
            })
        }
    }

    /* OnClick */

    handleOnClickToLogIn = () => {
        this.setState({
            firstVisit: false,
        })
    }

    handleOnClickToRegister = () => {
        this.props.history.replace('/register');
    }

    handleOnLogIn = () => {
        const {  rootStore } = this.props;
        const { emailInput, passwordInput, remember } = this.state;

        if(emailInput === ""){
            this.setState({
                isShowErrorEmail: true,
                errorEmailMsg: USER_ERROR.REQUIRED,
            })
        }

        if(passwordInput === ""){
            this.setState({
                isShowErrorPassword: true,
                errorPassordMsg: PASSWORD_ERROR.REQUIRED,
            })
        }

        if(passwordInput !== '' && emailInput !== '' && this.checkValidatePassword()){ 
            this.setState({
                isShowErrorEmail: false,
                isShowErrorPassword: false,
                errorEmailMsg: '',
                errorPassordMsg: '',
            })      
            const payload = {
                Email: emailInput,
                Password: passwordInput,
                RememberMe: remember
            }
            rootStore.account.signIn(payload, this.loginCallBack);
        } 
    }

    /* Other */

    checkValidatePassword = () => {
        
        const { isShowErrorPassword, errorPassordMsg , passwordInput } = this.state;

        if( passwordInput.length < 6 ){
            this.setState({
                isShowErrorPassword: true,
                errorPassordMsg: PASSWORD_ERROR.MIN_LENGTH,
            })
            return false;
        }
        if( !validatePWLowerCase(passwordInput)){
            this.setState({
                isShowErrorPassword: true,
                errorPassordMsg: PASSWORD_ERROR.ONE_LOWER_CASE,
            })
            return false;
        }
        if( !validatePWUpperCase(passwordInput)){
            this.setState({
                isShowErrorPassword: true,
                errorPassordMsg: PASSWORD_ERROR.ONE_UPPER_CASE,
            })
            return false;
        }
        if( !validatePWDigit(passwordInput)){
            this.setState({
                isShowErrorPassword: true,
                errorPassordMsg: PASSWORD_ERROR.ONE_DIGIT,
            })
            return false;
        }
        if( !validatePWAlpha(passwordInput)){
            this.setState({
                isShowErrorPassword: true,
                errorPassordMsg: PASSWORD_ERROR.ONE_ALPHANUMERIC,
            })
            return false;
        }
        return true;
    }

    checkEmailInput = () => {
        const { emailInput } = this.state;
        if(emailInput === ""){
            this.setState({
                isShowErrorEmail: true,
                errorEmailMsg: 'This field is required'
            });
        }

    }

    checkPasswordInput = () => {
        const { passwordInput } = this.state;
        if(passwordInput === ""){
            this.setState({
                isShowErrorPassword: true,
                errorPassordMsg: 'This field is required'
            });
        }
    }
    /* OnChange */

    handleCheckBoxChange = (e) => {
        this.setState({
            remember: e.target.checked,
        })
    }

    handleInputEmail = (e) => {
        this.setState({
            emailInput: e.target.value,
            isShowErrorEmail: false,
        },() => this.checkEmailInput());
    }

    handleInputPassword= (e) => {
        this.setState({
            passwordInput: e.target.value,
            isShowErrorPassword: false,
        }, () => this.checkPasswordInput());
    }

    onChangeShowPassword = () => {
        const { isShowPassWord } = this.state;
        this.setState({
            isShowPassWord : !isShowPassWord
        })
    }

    render(){
        const { remember, firstVisit, isShowErrorEmail, isShowErrorPassword , errorEmailMsg, errorPassordMsg } = this.state;

        return(
            <div className="vfsm-login-page-wrapper">
                < WelcomeHeader />
                <div className="vfsm-login-page-container-wrapper">
                {
                    firstVisit ? 
                    ( 
                        <div className="vfsm-login-form-first-visit-container">
                            <div className="vfsm-login-form-first-visit-content">
                                <span style={{'font-weight': 'bold'}}>VODAFONE </span>
                                <span> bracelet management site </span>
                            </div>
                            <div className="vfsm-login-form-first-visit-button">
                                <VodaButton text="LOG IN" onClick={this.handleOnClickToLogIn} />
                            </div>
                            {/* <div className="vfsm-login-form-first-visit-button">
                                <VodaButton text="REGISTER" onClick={this.handleOnClickToRegister} />
                            </div> */}
                        </div>

                    ) : 
                    (
                    <div className="vfsm-login-form-wrapper"> 
                            <div className="vfsm-login-form-first-visit-content">
                                <span style={{'font-weight': 'bold'}}>VODAFONE </span>
                                <span> bracelet management site </span>
                            </div>
                            <div className="vfsm-login-form-guid">
                                <span> Use a email account to log in </span>
                            </div>
                            <div className="vfsm-login-form-dash"></div>
                            <div className="vfsm-login-form">
                                <VodaInput 
                                    className="vfsm-login-input" 
                                    title="Email"
                                    width={100}
                                    height={50}
                                    placeholder="Type email" 
                                    onChange={this.handleInputEmail}
                                    onPressEnter={this.handleOnLogIn}
                                    value={this.state.emailInput}
                                    options={{
                                        isShowError:isShowErrorEmail,
                                        errorMsg: errorEmailMsg,
                                    }}
                                />
                                <VodaInput 
                                    className="vfsm-login-input"
                                    title="Password" 
                                    type="password"
                                    width={100}
                                    height={50}
                                    placeholder="Type password" 
                                    onChange={this.handleInputPassword}
                                    onPressEnter={this.handleOnLogIn}
                                    value={this.state.passwordInput}
                                    options={{
                                        isShowError:isShowErrorPassword,
                                        errorMsg: errorPassordMsg,
                                    }}
                                />
                                <div className="vfsm-login-form-button-box">
                                    <VodaButton 
                                        text="Login" 
                                        onClick={this.handleOnLogIn} 
                                    />
                                    <a href={this.forgotPwdUrl} > Forgot your password? </a>
                                </div>
                                <VodaCheckBox checked={remember} onChange= {this.handleCheckBoxChange} text="Remember me?"/>
                            </div>
                    </div>
                    )
                }
                </div>
                {/* <div className="vfsm-page-footer-wrapper">
                    <span className="vfsm-page-footer-contact">Contact</span>
                    <span className="vfsm-page-footer-humax"> Copyright © 2018 HUMAX Co., Ltd. All rights reserved.</span>
                </div> */}
            </div>
        )
    }
}


export default LoginForm;