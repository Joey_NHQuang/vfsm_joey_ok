import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';
import SmartDevice from '../../DeviceCard/images/SmartDevice.svg';
import { Avatar, Popover } from 'antd';

class Device extends React.Component {

    handleClick = (data) => {
        this.props.handleClick(data);
    }

    render() {

        const { data } = this.props;

        return (
            <div className="see-more-device">
                {
                    data.role === "ROOT"
                        ? <div className="root-show">
                            <div className="device-root" onClick={() => this.handleClick(data)}>
                                <Avatar shape="square" src={SmartDevice} />
                            </div>
                            <Popover placement="bottom" content={data.imei} trigger="hover">
                                <div className="device-id">
                                    <span>{data.imei}</span>
                                </div>
                            </Popover>
                        </div>
                        : <div className="sub-show">
                            <div className="device-sub" onClick={() => this.handleClick(data)}>
                                <Avatar shape="square" src={SmartDevice} />
                            </div>
                            <Popover placement="bottom" content={data.imei} trigger="hover">
                                <div className="device-id">
                                    <span>{data.imei}</span>
                                </div>
                            </Popover>
                        </div>
                }
            </div>

        )


    }
}

Device.PropTypes = {
    data: PropTypes.object.isRequired
}

export default Device;