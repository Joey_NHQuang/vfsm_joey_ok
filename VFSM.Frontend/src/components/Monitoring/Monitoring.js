import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
// import { Link } from 'react-router-dom';
// import { observer } from 'mobx-react';
import DatePicker from '../../common/DatePicker/DatePicker';
import moment from 'moment';
import Select from '../../common/Select/Select';
import { Input, Row, Col, Modal } from 'antd';
import VodaButton from '../../common/VodaButton/VodaButton';
import CustomerCard from './CustomerCard/CustomerCard';
import DeviceCard from './DeviceCard/DeviceCard';
import CustomerModal from './CustomerModal/CustomerModal'
import DeviceModal from './DeviceModal/DeviceModal'
import './styles.scss';
import { DateTime } from '../../core/utils';
import VodaBody from '../../common/VodaBody/VodaBody';

const { formatTime, formatTimeToSend } = DateTime;
const pageSize = 10;
@inject('rootStore')
@inject('apiStore')
@inject('menuRoleStore')
@observer
class Monitoring extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openCustomer: false,
            openDevice: false,
            disableInput: true,
            startDate: moment().subtract(6, 'months'),
            endDate: moment(),
            sortType: undefined,
            orderType: undefined,
            page: 1,
            searchType: undefined,
            searchInput: '',
            customerId: undefined,
            device: undefined
        };
        this.option = [
            {
                label: 'IMEI of device',
                value: 'imei'
            },
            {
                label: 'Customer ID',
                value: 'userId'
            },
            {
                label: 'Beacon MAC',
                value: 'mac'
            }
        ];
        this.optionSort = [
            {
                label: 'Customer ID',
                value: 'UserId'
            },
            {
                label: 'Mobile Type',
                value: 'MobileType'
            },
            {
                label: 'Created Date',
                value: 'CreatedDate'
            }
        ];
    }

    // Get all employee
    componentDidMount() {
        const fromDate = formatTimeToSend(this.state.startDate);
        const toDate = formatTimeToSend(this.state.endDate);
        const body = {
            fromDate: fromDate,
            toDate: toDate,
            CurrentPage: 1,
            PageSize: pageSize
        };
        this.props.rootStore.user.getAll(body);
    }

    // Change page
    changePage = (pageNumber) => {
        this.setState({ page: pageNumber }, () => this.handleSearch());
    }

    // Sorting
    handleSort = (value) => {
        if (!value) {
            this.setState({ sortType: value, orderType: undefined }, () => this.handleSearch());
        } else {
            this.setState({ sortType: value }, () => this.handleSearch());
        }
    }

    handleSortOrder = (type) => {
        this.setState({ orderType: type }, () => this.handleSearch());
    }

    // Change Search type
    handleChangeSearchType = (value) => {
        this.setState({ searchType: value });
        if (!value) {
            this.setState({ disableInput: true, searchInput: '' });
        } else {
            this.setState({ disableInput: false });
        }
    }

    // Change Input when search
    handleChangeSearchInput = (e) => {
        this.setState({ searchInput: e.target.value });
    }

    // Change From date
    handleChangeStartDate = (date) => {
        this.setState({
            startDate: date
        });
    }

    // Change To date
    handleChangeEndDate = (date) => {
        this.setState({
            endDate: date
        });
    }

    // Click search button
    handleSearch = () => {
        const { searchType, searchInput, page, startDate, endDate, sortType, orderType } = this.state;
        const body = {
            fromDate: formatTimeToSend(startDate),
            toDate: formatTimeToSend(endDate),
            CurrentPage: page,
            PageSize: pageSize,
            sort: sortType,
            sortAsc: orderType
        }
        body[searchType] = searchInput;
        this.props.rootStore.user.getAll(body);
    }

    // Close customer modal
    handleCloseCustomerModal = () => {
        this.setState({ openCustomer: false, customerId: undefined });
    };

    // Show customer modal
    handleShowCustomer = (customerId) => {
        this.setState({ openCustomer: true, customerId: customerId });
    }

    // Render modal view customer detail
    renderCustomerModal = () => {

        const { openCustomer, customerId } = this.state;
        if (openCustomer) {
            return (
                <Modal
                    width={693}
                    footer={null}
                    title="Customer Detail"
                    visible={openCustomer}
                    onCancel={this.handleCloseCustomerModal}
                >
                    <CustomerModal customerId={customerId} onClose={this.handleCloseCustomerModal} />
                </Modal>
            )
        }
        return null;
    };

    // Close Device modal
    handleCloseDeviceModal = () => {
        this.setState({ openDevice: false, device: undefined });
    };

    // Show device modal
    handleShowDevice = (device) => {
        this.setState({ openDevice: true, device: device });
    }

    // Render modal view Device detail
    renderDeviceModal = () => {

        const { openDevice, device } = this.state;

        if (openDevice) {
            return (
                <Modal
                    width={693}
                    footer={null}
                    title="Device Detail"
                    visible={openDevice}
                    onCancel={this.handleCloseDeviceModal}
                >
                    <DeviceModal device={device} onClose={this.handleCloseDeviceModal} />
                </Modal>
            )
        }
        return null;
    };

    render() {
        const { loading } = this.props.apiStore;
        const { menuRoles } = this.props.menuRoleStore;
        const { rootStore } = this.props;
        const customers = rootStore.user.users;

        const body =
            <Row gutter={16}>
                {
                    customers.items && customers.items.length > 0
                        ? customers.items.map(customer => {
                            return (<Col className="gutter-row" span={12} key={customer.userId}>
                                <div className="monitoring-child">
                                    <Row gutter={8}>
                                        <Col className="gutter-row time-column" span={3}>
                                            <span className="monitoring-time" key={customer.userId}>{formatTime(customer.createdDate)}</span>
                                        </Col>
                                        <Col className="gutter-row" span={8}>
                                            <div className="monitoring-customer">
                                                <CustomerCard id={customer.userId} name={customer.nickName} type={customer.mobileType} handleClickAvatar={this.handleShowCustomer} key={customer.userId} />
                                            </div>
                                        </Col>
                                        <Col className="gutter-row device-column" span={13}>
                                            <div className="monitoring-device">
                                                <DeviceCard userId={customer.userId} deviceList={customer.device} handleClickDevice={this.handleShowDevice} key={customer.userId} menuRole={menuRoles.monitoring} />
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                            </Col>)

                        })
                        : loading ? <div></div> : <div style={{ fontSize: '30px', fontWeight: 500, textAlign: 'center', padding: '50px', fontStyle: 'italic' }}>No found the result</div>
                }
            </Row>
        return (
            <div>
                <div className="row-end">
                    <DatePicker
                        label="From"
                        className="date-from"
                        selected={this.state.startDate}
                        onChange={this.handleChangeStartDate}
                    />
                    <DatePicker
                        label="To"
                        className="date-to ml-1"
                        selected={this.state.endDate}
                        onChange={this.handleChangeEndDate}
                    />
                    <Select className="ml-1" placeholder="Type: Search type" option={this.option} onChange={this.handleChangeSearchType} />
                    <Input disabled={this.state.disableInput} className="ml-1" placeholder="Search" onChange={this.handleChangeSearchInput} />
                    <VodaButton text="Search" iconType="Search" onClick={this.handleSearch} />
                </div>
                <VodaBody
                    optionSort={this.optionSort}
                    onChangeSort={this.handleSort}
                    body={body}
                    pageCount={customers.pageCount}
                    onChangePage={this.changePage}
                    onSortOrder={this.handleSortOrder}
                />
                {this.renderCustomerModal()}
                {this.renderDeviceModal()}
            </div >
        );
    }

};

export default Monitoring;
