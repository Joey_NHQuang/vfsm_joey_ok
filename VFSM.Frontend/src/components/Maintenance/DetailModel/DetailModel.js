import React from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import InfoBox from '../../../common/InfoBox/InfoBox';
import ButtonFooter from '../../../common/ButtonFooter/ButtonFooter';
import VodaInput from '../../../common/Input/VodaInput';
import { DateTime } from '../../../core/utils';
import Constant from  '../../../core/utils/Constant';
import Notification   from '../../../core/ui/Notification/Notification';
import './styles.scss';

const {formatTimeDetail} = DateTime;
const { USER_ERROR, API_STATUS} = Constant;
const { onOpenNotification } = Notification;
@inject('rootStore')
@inject('apiStore')
@inject('menuRoleStore')
@observer
class DetailModel extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            disabled: false,
            isInputNameShowError : false,
            isInputDescriptionShowError: false,
            newModelName : '',
            newModelDescription:  '',
            isEdit : false,
            isLoading : false,
        }
    }
    
    componentDidMount () {
        const {rootStore, id } = this.props;
        rootStore.model.getDetail(id);
    }

    hanldeCheckInputChange = () => {
        const { newModelName  } = this.state;
        if(newModelName  && newModelName !== ''){
            this.setState({
                disabled: false,
            })
        }
    }

    handleInputModelName = (value) =>{
        this.setState({
            newModelName: value.target.value
        }, this.hanldeCheckInputChange);
    }
    
    handleInputModelDescription = (value) =>{
        this.setState({
            newModelDescription: value.target.value
        }, this.hanldeCheckInputChange());
    }

    handleEditModelCallBack = () => {
        const { isEdit } = this.state;
        const status = this.props.rootStore.model.editModelStatus.status ? this.props.rootStore.model.editModelStatus.status : '';
        let editNoti = '', editStatus = '';
        if(status.toUpperCase() === API_STATUS.SUCCESS){
            this.setState({
                isEdit: false,
                disabled: false,
                isLoading: false,
            });
            editNoti = 'Edit Successful';
            editStatus = API_STATUS.SUCCESS;
        } else {
            this.setState({
                isEdit: false,
                disabled : false,
                isLoading: false
            });
            editNoti = 'Edit Unsuccessful';
            editStatus = API_STATUS.ERROR;
            this.handleClickCancelCallback();
        }
        this.openNotification(editNoti, editStatus);
    }

    handleSaveChange = () => {
        const { newModelName, newModelDescription } = this.state;
        const newModel = {
            modelId : this.props.rootStore.model.details.modelId,
            name : newModelName,
            description : newModelDescription
        }
        this.props.rootStore.model.editModel(newModel , this.handleEditModelCallBack);
        this.setState({
            disabled: true,
            isLoading: true,
            newModelDescription: '',
            newModelName: '',
        })
    }

    handleClickFirst = () => {
        const { isEdit } = this.state;
        this.setState((prevSate) => ({
            isEdit: {
                ...prevSate.isEdit,
                isEdit : !prevSate.isEdit
            },
       
        }), () => {
            if(!isEdit) {
                this.setState({
                    disabled: true,
                })
            } else {
                this.handleSaveChange();
            }
        });
    }

    handleCancel =() =>{
        const { isEdit } = this.state;
        const status = this.props.rootStore.model.editModelStatus.status ? this.props.rootStore.model.editModelStatus.status.toUpperCase() : '';
        const hasChange = status === 'SUCCESS' ? true : false ;
        this.setState({
            isEdit: false,
            disabled: false,
            newModelDescription: '',
            newModelName:  '', 
        }, () => this.props.handleClickCancelCallback()); 
       
    }

    /* notification */
    openNotification = (msg, status) =>{
        onOpenNotification(msg, status);
    }

    render(){
        const { id , rootStore} = this.props;
        const { isEdit, isLoading, isInputNameShowError, isInputDescriptionShowError  } = this.state;
        const { menuRoles } = this.props.menuRoleStore;
        const dataDetail = rootStore.model.details;
        const data = [
            
            {
                "title"  : 'Model ID',
                "content": dataDetail.modelId ? dataDetail.modelId : '--',
                'hasFooter': true,
                "hasInput" : false
            },
            {
                "title"  : 'Name',
                "content":  isEdit ? (<div className="modal-col-input-wapper">
                                        <VodaInput className="add-device-input" 
                                            placeholder={dataDetail.name ? dataDetail.name : 'Type'}
                                            onChange={this.handleInputModelName} value={this.state.newModelName}
                                            options={{
                                                maxLength: 24,
                                            }}
                                         />
                                        
                                     </div>) 
                                   : ( dataDetail.name ? <span className="modal-col-input-content">{dataDetail.name}</span> : '--'),
                'hasFooter': true,
                "hasInput" : isEdit
            },
            {
                "title"  : 'Description',
                "content":  isEdit ? (<div className="modal-col-input-wapper">
                                        <VodaInput className="add-device-input" 
                                            placeholder={dataDetail.description ? dataDetail.description : 'Type' } 
                                            onChange={this.handleInputModelDescription} value={this.state.newModelDescription}
                                            options={{
                                                maxLength: 50,
                                            }}
                                        />
                                     </div>) 
                                   : ( dataDetail.description ? <span className="modal-col-input-content" >{dataDetail.description}</span> : '--'),
                'hasFooter': true,
                "hasInput" : isEdit
            },
            {
                "title"  : 'Version number',
                "content": (dataDetail.versionMC60 || dataDetail.versionFirmware) ? 
                                (<span className="modal-col-input-content" >
                                    {dataDetail.versionMC60 ? dataDetail.versionMC60 + ' ' +'(MC60)' : ''}
                                    {dataDetail.versionMC60 && dataDetail.versionFirmware ? ' - ' : '' }
                                    {dataDetail.versionFirmware ? dataDetail.versionFirmware + ' ' +'(FIRMWARE)' : ''}
                                </span>)
                                : ('--'),
                'hasFooter': true,
                "hasInput" : false
            },
            {
                "title"  : 'Last updated',
                "content": dataDetail.lastUpdateDate ? formatTimeDetail(dataDetail.lastUpdateDate) : '--',
                'hasFooter': true,
                "hasInput" : false
            }
        ];

        const  {menuRole}  = this.props;
        const {loading} = this.props.apiStore;
        const displayEdit =  (menuRoles && menuRoles.maintenance !== 'operation.write') ? 'none' : 'block';
        return(
            <div className="modal-model-detail-wapper">
                <InfoBox data={data}/>
                {
                    menuRole && menuRole === 'operation.write' &&
                    <ButtonFooter text={ isEdit ? "Save" : "Edit" } 
                    className= {isEdit ? "model-detail-button" : "model-detail-button-edit"}
                    style={{display: displayEdit}}
                    disabled={ (menuRoles && menuRoles.maintenance !== 'operation.write') ? true : this.state.disabled} 
                    loading={loading}
                    handleClickCancel={this.handleCancel} 
                    handleClickFirst={this.handleClickFirst}/>
                }
            </div>
        )
    }
}

DetailModel.propTypes = {
    id: PropTypes.string.isRequired,
}

export default DetailModel;

