import React from 'react';
import {Card, Modal } from 'antd';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import DetailModel from '../DetailModel/DetailModel';
import IconSvg from '../../../common/icon/IconSvg';
import SelectBox from '../../../common/Select/Select';
import SmallSelect from '../../../common/SmallSelect/SmallSelect';
import ConfirmModal from '../../../common/ConfirmModal/ConfirmModal';

import './cardstyle.scss'
@inject('rootStore')
@observer
class ModelCard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isOpenModelDetail: false,
            isOpenConfirm: false,
            isConfirmDisable: false,
            idDelete: undefined,
            isLoading: false
        }
    }

    /* handle click menu */
    handleClickMenuCallBack =( eventName) => {
        if(eventName === 'Detail'){
            this.setState({
                isOpenModelDetail: true
            });
        } else if (eventName === 'Delete') {
            this.handleDeleteModel(this.props.modelId);
        }
    }

    /* */
    handleDeleteModel = (id) => {
        //this.props.rootStore.model.deleteModel(id);
        this.setState({
            isOpenConfirm: true,
            idDelete: id
        })
    }

    handleCloseConfirmModal = () => {
        this.setState({
            isOpenConfirm: false,
            idDelete: undefined,
            isLoading: false,
            isConfirmDisable: false,
        })
    }

    deleteModelCallBack = () =>{
        const deleteDetails = this.props.rootStore.model.details ? this.props.rootStore.model.details : {};
        if(deleteDetails) {
            if ( deleteDetails.status.toUpperCase() === 'SUCCESS') {
                this.setState({
                    isOpenConfirm:false,
                    isLoading: false
                })
            }
        } else {
            this.setState({
                isConfirmDisable: false,
                isLoading: false
            })
        }
     
    }

    handleConfirm = () => {
        this.props.rootStore.model.deleteModel(this.state.idDelete , this.deleteModelCallBack);
        this.setState({
            isConfirmDisable: true,
            isLoading: true
        })
    }

    /* */
    handleCloseModelModal = () => {
        this.setState({
            isOpenModelDetail: false
        });
    }

    handleClickCancelCallback =() => {
        this.setState({
            isOpenModelDetail: false
        });
    }

     /* open OTA page */
    handleClickOpenOTA = (modelId) => {
        this.props.rootStore.ota.setModelId(modelId);
        this.props.handleClickOpenOTACallBack(modelId);
    }

    renderModelDetail = () => {
        const { isOpenModelDetail } = this.state;
        if(isOpenModelDetail) {
            return (
                <Modal
                width={693}
                footer={null}
                title="Model Detail"
                visible={isOpenModelDetail}
                onCancel={this.handleCloseModelModal}
                >
                    <DetailModel id={this.props.modelId}
                        handleClickCancelCallback={this.handleClickCancelCallback}
                        menuRole={this.props.menuRole}
                    />
                </Modal>
            )
        }
        return null;
    }

    render() {
        const { isOpenConfirm, isConfirmDisable, isLoading} = this.state;
        const {data, modelId , menuRole} = this.props;
        const selectOption = menuRole === 'operation.write' ? [
            {
                "title" : "Detail",
                "className" : "select-option"
            },
            {
                "title" : "Delete",
                "className" : "select-option-delete"
            }

        ] : 
        [
            {
                "title" : "Detail",
                "className" : "select-option"
            },
        ]
        return (
            <div style={{'position': 'relative'}}>
                <Card 
                    className="model-card-detail" 
                    style={{
                        marginTop: '20px',
                        borderRadius: '2px',
                        width:'100%',
                        backgroundColor:'#F4F4F4'}}
                    onClick={ () => this.handleClickOpenOTA(modelId)}
                >
                    <div className="model-card-container" >
                        {data.name}
                    </div>
                </Card>
                <div className="model-card-icon-wapper">
                        <SmallSelect  selectOption={selectOption} handleClickMenuCallBack={this.handleClickMenuCallBack}/>
                </div>
                {this.renderModelDetail()}
                <ConfirmModal  
                    visible={isOpenConfirm}
                    confirmDisable={isConfirmDisable}
                    title={"Do you want to delete ?"} 
                    handleCloseModal={this.handleCloseConfirmModal} 
                    handleConfirm={this.handleConfirm}
                    loading={isLoading}
                />
            </div>
        );
    }
}

ModelCard.PropTypes = {
    data: PropTypes.object.isRequired,
    modelId: PropTypes.string.isRequired
}

export default ModelCard;