import React from 'react';
import { toJS } from "mobx";
import { inject, observer } from 'mobx-react';
import { Table, Input, InputNumber, Popconfirm, Form, Switch, Upload, Button, message, notification  } from 'antd';
import Select from './Select/Select';
import ConfirmModal from 'common/ConfirmModal/ConfirmModal';
import UploadFile from 'common/UploadFile/UploadFile';
import { validateFiles } from 'core/utils/validate';
import VodaButton from 'common/VodaButton/VodaButton';

import 'antd/dist/antd.css';
import './styles.scss';

const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);
class EditableCell extends React.Component {
    getInput = (value) => {
        let disabled = false;
        const { inputType, acceptFileUpload, onDoneUpload, onBeforeUpload } = this.props;
        switch (inputType) {
            case 'switch':
                return <Switch defaultChecked={value} />;
            case 'file':
                return (
                    <div>
                        <UploadFile
                            multiple={false}
                            onDone={onDoneUpload}
                            accept={acceptFileUpload}
                            onBeforeUpload={onBeforeUpload} />
                    </div>
                );

            default:
                return <Input />;
        }
    };

    render() {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            idRow,
            ...restProps
        } = this.props;
        return (
            <EditableContext.Consumer>
                {(form) => {
                    const { getFieldDecorator } = form;
                    return (
                        <td {...restProps}>
                            {editing ? (
                                <FormItem style={{ margin: 0 }}>
                                    {getFieldDecorator(dataIndex, {
                                        rules: [{
                                            required: true,
                                            message: `Please Input ${title}!`,
                                        }],
                                        initialValue: record[dataIndex],
                                    })(this.getInput(record[dataIndex]))}
                                </FormItem>
                            ) : restProps.children}
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        );
    }
}

@inject('apiStore')
@inject('rootStore')
@observer
class EditableTable extends React.Component {
    constructor(props) {
        super(props);
        this.props.onRef(this);
        // this.currentData = [...props.data];
        this.state = {
            data: toJS(props.data),
            status: '',
            idRow: '',
            form: '',
            statusConfirm: 'Delete',
            confirmMsg: '',
            openConfirm: false,
            record: {},
            file: {},
            tempData: [],
            idOnChange: 0,
            idOnFocus: 0,
            isOnEdit: false,
            isOnhightlight: false,
            tempDataDelete: [],
        };

        if (props.columns) {
            this.columns = toJS(props.columns);
            let selectOption = [];
            if (this.props.menuRole == 'operation.write') {
                selectOption = [
                    {
                        "title": "Edit",
                        "className": "select-option"
                    },
                    {
                        "title": "Delete",
                        "className": "select-option-delete"
                    }
                ];
            };
            
            if(this.props.menuRole == 'operation.write'){
                this.columns = [ ...this.columns, {
                title: '',
                render: (text, record) => {
                    const editable = this.isEditing(record);
                    return (
                        <div>
                            {editable ? (
                                <span className='action-row'>
                                    <EditableContext.Consumer>
                                        {form => (
                                            this.state.status === 'edit' ?
                                                <VodaButton className='save-row' text="Save" onClick={() => this.openConfirmSave(form, record.id, 'Save')} /> :
                                                <VodaButton className='save-row' text="Add" onClick={() => this.openConfirmSave(form, record.id, 'Add')} />
                                        )}
                                    </EditableContext.Consumer>
                                    <br/>
                                    <a className='cancel-row' onClick={() => this.cancel(record.version)}>Cancel</a>
                                </span>
                            ) : (
                                    <EditableContext.Consumer>
                                        {form => (
                                            <div className='vfsm-small-select-circle'>
                                                <Select selectOption={selectOption} handleClickMenuCallBack={this.handleClickEditOrDelete} record={record} form={form} />
                                            </div>  
                                        )}
                                    </EditableContext.Consumer>
                                )}
                        </div>
                    );
                },
            }];
            }
        }
    }

    componentDidMount = () => {

    }

    handleClickAdd = () => {
        const { status } = this.state;
        if( status !== 'add'){
            let newData = [];
            this.props.category === 'MC60'? newData = toJS(this.props.rootStore.ota.mc60) : newData = toJS(this.props.rootStore.ota.bracelet);
            if (newData.length > 0) {
                newData.unshift({ ...newData[0] });
                newData[0].id = 0;
                newData[0].modelId = this.props.rootStore.ota.modelId;
                newData[0].category = this.props.category;
                newData[0].isActive = false;
                newData[0].otaUrl = '';
                newData[0].notEdit.forEach(element => {
                    newData[0][element] = '';
                });
            } else {
                var obj = {};
                this.columns.forEach(element => {
                    obj[element.key] = '';
                });
                obj.id = 0;
                obj.modelId = this.props.rootStore.ota.modelId;
                obj.category = this.props.category;
                obj.isActive = false;
                newData.push(obj);
            }
            this.props.rootStore.ota.setListData(newData, this.props.category);
            this.setState({
                idRow: 0,
                status: 'add',
                isOnEdit: true,
                idOnFocus: 0,
            });
            this.props.checkDisableAddButton(true);
        }
    }

    handleClickEditOrDelete = (eventName, record, form) => {
        let data = [];
        this.props.category === 'MC60'? data = toJS(this.props.rootStore.ota.mc60) : data = toJS(this.props.rootStore.ota.bracelet);
        if (eventName.toUpperCase() === 'EDIT') {
            data.forEach(e => {
                if (e.id === record.id) {
                    e&&e.notEdit&&e.notEdit.forEach(element => {
                        e[element] = '';
                    });
                }
            });
            const index = data.findIndex(item => record.id === item.id);
            this.setState({
                idRow: record.id,
                form: form,
                status: 'edit',
                isOnEdit: true,
                idOnFocus:  (index >-1) ? index : 0,
                confirmMsg : 'Do you want to save the changes?'
            });
            this.props.checkDisableAddButton(true);
        }
        else if (eventName.toUpperCase() === 'DELETE') {
            this.setState({
                idRow: record.id,
                openConfirm: true,
                statusConfirm: 'Delete',
                form: form,
                record: record,
                status: 'delete',
                confirmMsg: 'Are you sure you want to delete selected item?'
            });
        }
    }
    isEditing = (record) => {
        return record.id === this.state.idRow&&this.state.status!='delete';
    };

    handleConfirm = () => {
        this.setState({ openConfirm: false });
        const { statusConfirm } = this.state;
        switch (statusConfirm) {
            case 'Delete':
                this.deleteOTA();
                break;
            case 'Save':
                this.save();
                // Call back function Edit with API
                break;
            case 'Add':
                this.save();
                // Call back function Edit with API
                break;
            default:
                break;
        }

    }
    handleCloseModal = () => {
        this.setState({ openConfirm: false });
    }
    // action row
    openConfirmSave = (form, idRow, status) => {
        form.validateFields((error, row) => {
            if (error) {
                return;
            } else {
                this.setState({
                    openConfirm: true,
                    confirmMsg : 'Do you want to save the changes?',
                    statusConfirm: status,
                    form: form,
                    idRow: idRow
                });
            }
        });
    }

    getRowClassName = ( record, index ) => {
        const { idOnChange, isOnhightlight, idOnFocus, isOnEdit, status } = this.state;
        if(idOnChange === index ){
            if (isOnhightlight && !isOnEdit) {
                return 'vfsm-row-hightlight';
            }
        }
        if(status.toUpperCase() === 'EDIT' || status.toUpperCase() === 'ADD' ){
            if (idOnFocus === record.id || idOnFocus === index){
                if (isOnEdit) {
                    return 'vfsm-row-forcus';
                }
            } else {
                return 'vfsm-row-blur'
            }
        }
        return 'editable-row';
    }

    onCloseNotification = () => {
        this.setState({
            isOnhightlight: false,
        })
    }

    onHandleOpenNotification = (customMsg, msg) => {
        notification.open({
            message: '',
            description: customMsg ? customMsg : '',
            placement: 'bottomRight',
            className: msg.toUpperCase() === 'SUCCESS' ? 'vfsm-ota-notification' : 'vfsm-ota-notification-error',
            onClose: () => this.onCloseNotification(),
        });
    }

    saveCallBack = (msg, customM) => {
        const { tempData, status} = this.state;
        let customMsg = '';
        if(msg && msg.toUpperCase() === 'SUCCESS'){
            this.setState({
                data: tempData,
                isOnhightlight: true,
                status: '',
            })
            customMsg = customM
        }
        else {
            customMsg = customM
            this.setState({ isOnhightlight: status === 'edit' ? true : false });
            this.cancel();
        }
        this.props.checkDisableAddButton(false);
        this.onHandleOpenNotification(customMsg, msg);
    }

    deleteCallback = (msg) => {
        const { tempDataDelete} = this.state;
        let data = [];
        this.props.category === 'MC60'? data = toJS(this.props.rootStore.ota.mc60) : data = toJS(this.props.rootStore.ota.bracelet);
        let customMsg = '';
        if(msg && msg.toUpperCase() === 'SUCCESS'){
            this.setState({
                data: tempDataDelete
            })
            customMsg = 'Delete Successful!'
        }
        else {
            customMsg = 'Delete Unsuccessful!'
        }
        this.onHandleOpenNotification(customMsg, msg);
    }

    checkInputField = (data) => {
        if(data && data !== ''  && data !== null ) {
            return true;
        }
        return false;
    }

    saveEditRecord = (newRecord, oldDataRow, callback) => {
        // Call api edit ota
        let bvalid =  false;
        if(newRecord) {
            newRecord.modelId = oldDataRow.modelId;
            newRecord.category = oldDataRow.category;
            if (newRecord.modelId !== undefined){
                if(newRecord.category === 'MC60'&&newRecord.mappingVersion !== undefined ){
                    bvalid = this.checkInputField(newRecord.mappingVersion);
                }
            }
        }
        if(newRecord.status === 'add'){
            if(newRecord.category === 'MC60'&&bvalid){
                this.props.rootStore.ota.createOta(newRecord, callback);
            }
            if (newRecord.category === 'FIRMWARE') {
                this.props.rootStore.ota.createOta(newRecord, callback);
            }
        } else if ( newRecord.status === 'edit' ){
            newRecord.version = this.state.idRow;
            if(newRecord.category === 'MC60'&&bvalid){
                this.props.rootStore.ota.editOta(newRecord, callback, this.props.category);
            } 
            if (newRecord.category === 'FIRMWARE') {
                this.props.rootStore.ota.editOta(newRecord, callback, this.props.category);
            }
        }
    }

    deleteOTA = () => {
        const { form, idRow, record } = this.state;
        let data = [];
        this.props.category === 'MC60'? data = toJS(this.props.rootStore.ota.mc60) : data = toJS(this.props.rootStore.ota.bracelet);
        let deleteRecord = {};
        form.validateFields((error, row) => {
            deleteRecord = row;
            if (error) {
                return;
            }
            let newData = data;
            const index = newData.findIndex(item => item.id === idRow);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1);
            }
            // save draft.
            this.setState({ 
                tempDataDelete: newData,
                idRow: '',
            });
            
        });
        // Call back function delete with API
        this.props.deleteRecord(record, this.deleteCallback);
    }

    save = () => {
        const { form, idRow, status, file } = this.state;
        let data = [];
        this.props.category === 'MC60'? data = toJS(this.props.rootStore.ota.mc60) : data = toJS(this.props.rootStore.ota.bracelet);
        const oldData = data;
        let newRecord = {};
        const oldDataRow = oldData[0];
        form.validateFields((error, row) => {
            newRecord = row;
            if (error) {
                return;
            }
            let newData = data;
            const index = newData.findIndex(item => idRow === item.id);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });
                this.setState({ tempData: newData, idOnChange: index, idRow: '', status: 'edit' });
            } else {
                newData.unshift(row);
                this.setState({ tempData: newData, idOnChange: 0, idRow: '', status: 'add' });
            }
        });
        if (file) {
            newRecord.file = file;
        }
        newRecord.status = status;
        // newRecord.version = idRow;
        this.saveEditRecord(newRecord, oldDataRow, this.saveCallBack);
    }

    cancel = () => {
        this.props.checkDisableAddButton(false);
        this.props.rootStore.ota.cancelAdd(this.props.category);
        this.setState({
            idRow: '',
            status: '',
            isOnEdit: false,
        });
    };
    // end action row

    onDoneUpload = (file) => {
        let {idRow} = this.state;
        this.props.rootStore.ota.setFileSize(this.props.category, idRow, file.size);
        this.setState({ file: file });
    }
    onBeforeUpload = (files) => {
        const { acceptFileUpload } = this.props;
        if (acceptFileUpload === '.img') {
            const allowFiles = ['.img'];
            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                if (!validateFiles(file, allowFiles)) {
                    this.setState({ inValidFileImport: true });
                    return false;
                }
            }
            this.setState({ inValidFileImport: false });
            return true;
        }
        return true
    }

    render() {
        const { openConfirm, status, confirmMsg } = this.state;
        const { acceptFileUpload, menuRole } = this.props;
        const { loading } = this.props.apiStore;
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell,
            },
        };
        // if(this.props.data) {
        //     this.setState({ data: this.props.data });
        // }
        const columns = this.columns.map((col) => {
            if (!col.editable) {
                return col;
            }
            if (col.key === 'version' && this.state.status === 'edit') {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputType: col.type,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                    acceptFileUpload: acceptFileUpload,
                    onDoneUpload: this.onDoneUpload,
                    onBeforeUpload: this.onBeforeUpload,
                }),
            };
        });

        const mc60 = this.props.rootStore.ota.mc60;
        const bracelet = this.props.rootStore.ota.bracelet;
        return (
            <div>
                <Table
                    className="vfsm-editable-table-ota"
                    components={components}
                    bordered
                    dataSource={this.props.category === 'MC60'? mc60 : bracelet}
                    columns={columns}
                    rowClassName = {this.getRowClassName}
                    scroll={{ y: 440 }}
                    loading={loading}
                    pagination={false}
                />
                <ConfirmModal
                    visible={openConfirm}
                    title={confirmMsg ? confirmMsg : 'Do you want to save the changes?' }
                    handleConfirm={this.handleConfirm}
                    handleCloseModal={this.handleCloseModal} />
            </div>
        );
    }
}

export default EditableTable;