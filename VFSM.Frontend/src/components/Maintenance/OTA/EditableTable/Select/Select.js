import React from 'react';
import { Menu, Dropdown, Popconfirm } from 'antd';


import IconSvg from 'common/icon/IconSvg';
import './styles.scss'

export default class Select extends React.Component {

    constructor(props) {
        super(props);
    }

    handleClickMenu = (eventName, record) => {
        this.props.handleClickMenuCallBack(eventName, this.props.record, this.props.form);
    }
    menu = (
        <Menu style={{ width: 130 }}>
            {
                this.props.selectOption.map((prop, index) =>
                    <Menu.Item key={index} className={prop.className} disabled={prop.disabled} onClick={() => this.handleClickMenu(prop.title)}>
                        <div className={prop.className}>
                            {prop.title}
                        </div>
                    </Menu.Item>
                )
            }
        </Menu>
    )

    render() {

        return (
            <div className="small-select-wapper">
                <Dropdown overlay={this.menu} trigger={['click']} placement="topLeft">
                    <IconSvg name="More" className="model-card-icon" />
                </Dropdown>
            </div>
        )
    }
}
