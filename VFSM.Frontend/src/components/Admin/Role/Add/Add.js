
import React from 'react';
import { inject, observer } from 'mobx-react';
import { Input } from 'antd';
import Notification   from '../../../../core/ui/Notification/Notification';
import InfoBox from 'common/InfoBox/InfoBox';
import ButtonFooter from 'common/ButtonFooter/ButtonFooter';
import VodaInput from 'common/Input/VodaInput';
import SelectBox from 'common/Select/Select';
import { checkInputExit} from '../../../../core/utils/validate';
import Roles from 'core/utils/RoleTable';
import Constant from  '../../../../core/utils/Constant';
import './styles.scss';

const { role, page } = Roles;
const { TextArea } = Input;
const { USER_ERROR, API_STATUS} = Constant;
const { onOpenNotification } = Notification;
@inject('rootStore')
@inject('apiStore')
@observer
class Add extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            disabled: true,
            isShowError: false,
            errorMsg: '',
            // dashboardPolicy: role.Hide,
            monitoringPolicy: role.Hide,
            deviceEventPolicy: role.Hide,
            maintenancePolicy: role.Hide,
            // reportPolicy: role.Hide,
            adminPolicy: role.Hide,
            userPolicy: role.Hide,
            rolePolicy: role.Hide,
            // testPolicy: role.Hide,
            name: '',
            description: ''
        }
        this.roleOption = [
            {
                value: role.Read,
                label: 'Read'
            },
            {
                value: role.Write,
                label: 'Write'
            },
            {
                value: role.Hide,
                label: 'Hide'
            }
        ];
    }

    /**/
    handleCheckInput = () => {
        const { name, isShowError} = this.state;
        if(!checkInputExit(name)){
            this.setState({
                isShowError: true,
                disabled: true,
                errorMsg: USER_ERROR.REQUIRED, 
            })
        } else {
            this.setState({
                isShowError: false,
                disabled: false,
                errorMsg: USER_ERROR.EMPTY, 
            })
        }
    }

    // handleInputDas = (value) => {
    //     this.setState({
    //         dashboardPolicy: value,
    //         isShowError: false,
    //         disabled: false,
    //     }, this.handleCheckInput);
    // }
    
    handleInputMoni = (value) => {
        this.setState({
            monitoringPolicy: value,
            isShowError: false,
            disabled: false,
        }, this.handleCheckInput);
    }
    handleInputDE = (value) => {
        this.setState({
            deviceEventPolicy: value,
            isShowError: false,
            disabled: false,
        }, this.handleCheckInput);
    }
    handleInputMain = (value) => {
        this.setState({
            maintenancePolicy: value,
            isShowError: false,
            disabled: false,
        }, this.handleCheckInput);
    }
    // handleInputRep = (value) => {
    //     this.setState({
    //         // reportPolicy: value,
    //         isShowError: false,
    //         disabled: false,
    //     }, this.handleCheckInput);
    // }
    handleInputUser = (value) => {
        this.setState({
            userPolicy: value,
            isShowError: false,
            disabled: false,
        }, this.handleCheckInput);
    }
    handleInputRole = (value) => {
        this.setState({
            rolePolicy: value,
            isShowError: false,
            disabled: false,
        }, this.handleCheckInput);
    }
    // handleInputTest = (value) => {
    //     this.setState({
    //         testPolicy: value,
    //         isShowError: false,
    //         disabled: false,
    //     }, this.handleCheckInput);
    // }
    handleInputDescription = (value) => {
        this.setState({
            description: value.target.value 
        }, this.handleCheckInput)
    }
    handleInputName = (value) => {
        this.setState({
            name: value.target.value 
        }, () => this.handleCheckInput())
    }
    /**/

    handleCancel = () => {
        this.setState({
            name: '',
            description: '',
            // dashboardPolicy: role.Hide,
            monitoringPolicy: role.Hide,
            deviceEventPolicy: role.Hide,
            maintenancePolicy: role.Hide,
            // reportPolicy: role.Hide,
            adminPolicy: role.Hide,
            userPolicy: role.Hide,
            rolePolicy: role.Hide,
            // testPolicy: role.Hide,
            isShowError: false,
            disabled : true
        },this.props.handleCancelCallBack());
    }

    handleAddModelCallBack = () => {
        const addStatus = this.props.rootStore.role.addStatus ? this.props.rootStore.role.addStatus : '';
        let addNoti = '', addStt = '';
        if(addStatus.toUpperCase() === API_STATUS.SUCCESS) {
            this.setState({
                isShowError: false,
                disabled: true,
                errorMsg : USER_ERROR.EMPTY,
                name: '',
                description: '',
                // dashboardPolicy: role.Hide,
                monitoringPolicy: role.Hide,
                deviceEventPolicy: role.Hide,
                maintenancePolicy: role.Hide,
                // reportPolicy: role.Hide,
                adminPolicy: role.Hide,
                userPolicy: role.Hide,
                rolePolicy: role.Hide,
                // testPolicy: role.Hide,
                disabled : true
            },this.handleCancel());
            addNoti = 'Add Successfull';
            addStt = API_STATUS.SUCCESS;
        } else {
            this.setState({
                isShowError: true,
                disabled: true,
                errorMsg : "This name has exited"
            });
            addNoti = 'Add Unsuccessfull';
            addStt = API_STATUS.ERROR;
        }
        this.openNotification (addNoti, addStt);
    }

    openNotification = (msg, status) =>{
        onOpenNotification(msg, status);
    }

    getAdminoperationID = () => {
        const { userPolicy, rolePolicy } = this.state;
        if( userPolicy === role.Hide && rolePolicy === role.Hide){
            return role.Hide;
        }
        else if(userPolicy === role.Write && rolePolicy === role.Write) {
            return role.Write;
        } else {
            return role.Read;
        }
    }

    handleClickFirst = () => {
        // add new role
        const { name, description, monitoringPolicy, deviceEventPolicy, maintenancePolicy, adminPolicy, userPolicy,rolePolicy } = this.state;
        if (name.length === 1) {
            this.setState({
                isShowError: true,
                disabled: true,
                errorMsg : "The Name must be at least 2 and at max 256 characters long."
            });
            return false;
        }
        const adminRole = this.getAdminoperationID();
        const newModel = {
            name : name,
            description : description,
            pages: [
                // {
            //         Id: '22784ce6-2222-42c2-af5c-2536667d8991', 
            //         Name: 'Dashboard', 
            //         OperationId: dashboardPolicy
            //     },
                {
                    Id: '22784ce6-2222-42c2-af5c-2536667d8992', 
                    Name: 'Monitoring', 
                    OperationId: monitoringPolicy
                },
                {
                    Id: '22784ce6-2222-42c2-af5c-2536667d8993', 
                    Name: 'Device Event', 
                    OperationId: deviceEventPolicy
                },
                {
                    Id: '22784ce6-2222-42c2-af5c-2536667d8994', 
                    Name: 'Maintenance', 
                    OperationId: maintenancePolicy
                },
                // {
                //     Id: '22784ce6-2222-42c2-af5c-2536667d8995', 
                //     Name: 'Report', 
                //     OperationId: reportPolicy
                // },
                {
                    Id: '22784ce6-2222-42c2-af5c-2536667d8996', 
                    Name: 'Admin', 
                    OperationId: adminRole,
                },
                {
                    Id: '22784ce6-2222-42c2-af5c-2536667d8997', 
                    Name: 'User', 
                    OperationId: userPolicy
                },
                {
                    Id: '22784ce6-2222-42c2-af5c-2536667d8998', 
                    Name: 'Role', 
                    OperationId: rolePolicy
                },
                // {
                //     Id: '22784ce6-2222-42c2-af5c-2536667d8999', 
                //     Name: 'Tester', 
                //     OperationId: testPolicy
                // }
            ]
        }
        this.setState({
            disabled: true,
            isShowError: false
        })
        this.props.rootStore.role.addNew(newModel , this.handleAddModelCallBack);
    }

    render(){
        const { isShowError, errorMsg } = this.state;
        const { loading } = this.props.apiStore;
        const hData = [
            {      
                "title": "Name",
                "content" :  <VodaInput 
                                className="add-device-input" 
                                placeholder="Name Role" 
                                onChange={this.handleInputName} 
                                value={this.state.name}
                                options={{
                                    maxLength: 10,
                                    minLength: 2,
                                    isShowError: isShowError,
                                    errorMsg: errorMsg
                                }}
                              />,
                "hasInput" : true
            },
            {      
                "title": "Description",
                "content" :  <VodaInput 
                                className="add-device-input" 
                                placeholder="Explanation of the Role" 
                                onChange={this.handleInputDescription} 
                                value={this.state.description}
                                options={{
                                    maxLength: 100,
                                }}
                              />,
                "hasInput" : true
            }   
        ];
        const dData = [
            // {
            //     "title": "Dashboard",
            //     "content" :
            //                 <SelectBox className="add-device-input" 
            //                     onChange={this.handleInputDas} 
            //                     value={this.state.dashboardPolicy}
            //                     option={this.roleOption}
            //                     allowClear={false}
            //                 />,
            //     "hasInput" : true
            // },
            {
                "title": "Monitoring",
                "content" :
                            <SelectBox className="add-device-input" 
                                onChange={this.handleInputMoni} 
                                value={this.state.monitoringPolicy}
                                option={this.roleOption}
                                allowClear={false}
                            />,
                "hasInput" : true
            },
            {
                "title": "Device Event",
                "content" :
                            <SelectBox className="add-device-input" 
                                onChange={this.handleInputDE} 
                                value={this.state.deviceEventPolicy}
                                option={this.roleOption}
                                allowClear={false}
                            />,
                "hasInput" : true
            },
            {
                "title": "Maintainance",
                "content" :
                            <SelectBox className="add-device-input" 
                                onChange={this.handleInputMain} 
                                value={this.state.maintenancePolicy}
                                option={this.roleOption}
                                allowClear={false}
                            />,
                "hasInput" : true
            },
            // {
            //     "title": "Report",
            //     "content" :
            //                 <SelectBox className="add-device-input" 
            //                     onChange={this.handleInputRep} 
            //                     value={this.state.reportPolicy}
            //                     option={this.roleOption}
            //                     allowClear={false}
            //                 />,
            //     "hasInput" : true
            // },

            {
                "title": "User",
                "content" :
                    <SelectBox className="add-device-input" 
                        onChange={this.handleInputUser} 
                        value={this.state.userPolicy}
                        option={this.roleOption}
                        allowClear={false}
                    />,
                "hasInput" : true
            },
            {
                "title": "Role & policy",
                "content" :
                    <SelectBox className="add-device-input" 
                        onChange={this.handleInputRole} 
                        value={this.state.rolePolicy}
                        option={this.roleOption}
                        allowClear={false}
                    />,
                "hasInput" : true
            }
        ]
        return(
            <div>
                <div className="modal-add-model-wapper">
                    <InfoBox data={hData} />
                    <div className="vfsm-add-new-role-title">
                        <span>Set role for page: </span>
                    </div>
                </div>
                <div className="vfsm-add-new-role-content">
                        <InfoBox data={dData} />
                </div>
                <ButtonFooter 
                    text="Create" 
                    id="vfsm-button-footer-modal-save"
                    className="model-add-button"
                    disabled={this.state.disabled}
                    loading={loading}
                    handleClickCancel={this.handleCancel}
                    handleClickFirst={this.handleClickFirst}
                />
            </div>
        )
    }
}

export default  Add;

