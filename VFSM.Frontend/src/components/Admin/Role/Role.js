import React from 'react';
import { inject, observer } from 'mobx-react';
import { toJS } from "mobx";
import { Modal, Input, Col, Row } from 'antd';
import moment from 'moment';
import Card from 'common/Card/Card';
import VodaButton from 'common/VodaButton/VodaButton';
import DatePicker from 'common/DatePicker/DatePicker';
import Select from 'common/Select/Select';
import Add from './Add/Add';
import AddCard from 'common/AddCard/AddCard';
import IconSvg from 'common/icon/IconSvg';
import Detail from './Detail/Detail';
import VodaBody  from '../../../common/VodaBody/VodaBody';
import ConfirmModal from 'common/ConfirmModal/ConfirmModal';
import { DateTime } from 'core/utils';
import Constant from  '../../../../src/core/utils/Constant';
import Notification   from '../../../../src/core/ui/Notification/Notification';
import './styles.scss';


const { API_STATUS } = Constant;
const { formatTime, formatTimeToSend } = DateTime;
const { onOpenNotification } = Notification;
const pageSize = 100;
@inject('rootStore')
@inject('apiStore')
@inject('menuRoleStore')
@observer
class Role extends React.Component {

    constructor(props){
        super(props);
        this.state={
            isOpenDetail: false,
            isOpenAdd: false,
            openConfirm: false,
            idOTA: null,
            isopenOTA : false,
            openModelDetail: false,
            idDetail: null,
            searchInput: '',
            sortType: 'modelId',
            sortAsc: true,
            detailId: null,
            roleDetailTitle: '',
            disableOrderSort: true
        }
    }

    componentDidMount() {
        const { sort, sortAsc } =  this.state;
        const param = {
            CurrentPage: 1,
            PageSize: pageSize,
            sort: sort,
            sortAsc: sortAsc
        };
        this.props.rootStore.role.search();
    }

    /*  CallBack funcs*/
    //open OTA page callback
    handleClickOpenDetail = (id, name) => {
        this.props.rootStore.role.getDetail(id);
        this.setState({
            isOpenDetail: true,
            detailId: id,
            roleDetailTitle: name
        })
    }

    handleBackToModelsCallBack = () => {
        this.setState({isopenOTA: false})
    }

    handleCancelCallBack = () => {
        this.setState({
            isOpenAdd: false
        })
    }
    
    handleCloseModelModal = () => {
        this.setState({ openModelDetail : false })
    }

    handleChangeStartDate = (value) => {
        this.setState({
            startDate: value
        });
    }

    handleChangeEndDate = (value) => {
        this.setState({
            endDateDate: value
        });
    }

    handleChangeSortType = (value) => {
        this.setState({
            sortType : value
        }, () => this.handleClickSearch());
        if (!value) {
            this.setState({ disableOrderSort: true });
        } else {
            this.setState({ disableOrderSort: false });
        }
    }

    handleInputOnChange = (value) => {
        this.setState({
            searchInput: value.target.value
        }, );
    }

    handleOnPressEnter =(value)=> {
        this.setState({
            searchInput: value.target.value
        }, () => this.handleClickSearch());
    }

    handleClickSearch = () => {
        const { sort, sortAsc, searchInput } =  this.state;
        const param = {
            CurrentPage: 1,
            PageSize: pageSize,
            sort: sort,
            sortAsc: sortAsc,
            email: searchInput
        };
       this.props.rootStore.role.filterDataRender(searchInput);
        
    } 

    handleClickDeleteCard = (id) => {
        this.setState({
            openConfirm: true,
            detailId: id
        })
    }

    handleConfirmDelete = () => {
        const {detailId} = this.state;
        if(detailId){
            this.props.rootStore.role.remove(detailId, this.handleDeleteCallBack);
        }
    }

    handleDeleteCallBack = (msg) => {
        let removeStatus = '', msgNoti = '';
        if(msg.toUpperCase() === API_STATUS.SUCCESS ){
            msgNoti = 'Delete Successfull';
            removeStatus = API_STATUS.SUCCESS;
        } else {
            msgNoti = 'Delete Unsuccessfull';
            removeStatus = API_STATUS.ERROR;
        }
        this.handleCloseConfirmModal();
        this.openNotification(msgNoti ,removeStatus);
    }

    handleCloseConfirmModal = () => {
        this.setState({
            openConfirm: false,
        })
    }

    /* Sync after adding */
    handleSyncCallBack = () => {
        //this.handleClickSearch();
    }

    handleCloseAdd = () =>{ 
        this.setState({
            isOpenAdd: false
        });
    }

    handleAddModel = () =>{
        this.setState({
            isOpenAdd: true
        })
    }

    handleCloseDetail = () => {
        this.setState({
            isOpenDetail: false
        });
    }

    handleSortOrder = (value) => {
        this.setState({
            sortAsc : value
        }, () => this.handleClickSearch());
    }
    /* Notification */
    openNotification = (message, status) => {
        onOpenNotification(message,status);
    }

    /* render Sub*/

    renderDetailRole = () => {
        const {isOpenDetail} = this.state;
        if(isOpenDetail){
            return (
                <Modal
                width={693}
                footer={null}
                title={this.state.roleDetailTitle+' Policy'}
                visible={isOpenDetail}
                onCancel={this.handleCloseDetail}
                className="vfsm-detail-role-modal"
            >
                <Detail
                 handleClickCancelCallback={this.handleCloseDetail}/>
            </Modal >
            )
        }
        return null;
    }

    renderAddRole = () => {
        const { isOpenAdd } = this.state;
        if (isOpenAdd){
            return (
                <Modal 
                    width={693}
                    footer={null}
                    title="Add new Role"
                    visible={isOpenAdd}
                    onCancel={this.handleCloseAdd}
                    className="vfsm-add-new-role-modal"
                >
                <Add 
                    handleCancelCallBack={this.handleCancelCallBack} 
                />
                </Modal>
            )
        }
        return null;
    }

    renderConfirmBox = () => {
        const { openConfirm } = this.state;
        if( openConfirm ) {
            return (
                <ConfirmModal
                visible={openConfirm}
                title='Are you sure you want to remove this Role?'
                handleConfirm={this.handleConfirmDelete}
                handleCloseModal={this.handleCloseConfirmModal} 
                />
            )
        } 
        return null;
    }

    render() {
        const { loading } = this.props.apiStore;
        const items = this.props.rootStore.role.roles;
        const detailId = this.state.detailId;
        const { menuRoles } = this.props.menuRoleStore;
        const dataFiltered = items ? items : [] ;
        const optionSort = [
            // {
            //     label: 'User Name',
            //     value: 'USER_NAME'
            // },
            // {
            //     label: 'Email',
            //     value: 'EMAIL'
            // },
        ];
        const selectOption = [
            {
                "title" : "Detail",
                "className" : "select-option"
            },
            {
                "title" : "Remove",
                "className" : "select-option-delete"
            }

        ]

        const body =  
        <Row gutter={24} style={{paddingBottom: 20}}>
        {
            dataFiltered && dataFiltered.length > 0 
            ? (
                <div className="vfsm-role-body-container">
                    {
                        dataFiltered.map( (prop, index) =>
                            <Col className="gutter-row" span={6}  >
                                <Card 
                                    data={prop}
                                    Id={prop.id}
                                    key={index}
                                    selectOption={selectOption}
                                    handleClickOpenDetail = {this.handleClickOpenDetail}
                                    handleClickMenuCallBack = {this.handleClickMenuCallBack}
                                    handleClickDeleteCard={this.handleClickDeleteCard}
                                    menuRole={menuRoles.rolePolicy}
                                    exception = {prop.name.toUpperCase()  === 'ADMIN' ? true : false}
                                />
                            </Col>
                        )
                    }
                    {
                        menuRoles && menuRoles.rolePolicy == 'operation.write' ? 
                        <Col className="gutter-row" span={6} >
                            <AddCard handleClick={this.handleAddModel} />
                        </Col>
                        : null
                    }
                </div>
                )   
            :
            (
                loading ? <div></div> :<div style={{ fontSize: '30px', fontWeight: 500, textAlign: 'center', padding: '50px', fontStyle: 'italic' }}>No found the result</div>
            ) 
        }
        </Row>

        return (
            <div className="vfsm-role-container">
                <div className="vfsm-role-header">
                    <div className="row-end">
                        <Input  style={{marginLeft: '15px', width: '271px'}}
                            placeholder={"Search by Name"}
                            onPressEnter={this.handleOnPressEnter}
                            onChange={this.handleInputOnChange} />
                        <VodaButton  
                            text="Search" iconType="Search" 
                            onClick={this.handleClickSearch} 
                            disabled={false} 
                            onPressEnter={this.handleClickSearch}
                        />
                    </div>
                </div>
                <div className="vfsm-role-body">
                    <VodaBody 
                        onChangeSort={this.handleChangeSortType}
                        body={body}
                        onSortOrder={this.handleSortOrder}
                    />
                </div>
                {this.renderAddRole()}
                {this.renderDetailRole()}
                {this.renderConfirmBox()}
            </div>
        )
    }
}

export default Role;