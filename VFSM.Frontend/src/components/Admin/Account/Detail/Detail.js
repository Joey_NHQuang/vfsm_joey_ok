import React from 'react';
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import InfoBox from 'common/InfoBox/InfoBox';
import ButtonFooter from 'common/ButtonFooter/ButtonFooter';
import VodaInput from 'common/Input/VodaInput';
import SelectBox from 'common/Select/Select';
import Notification   from '../../../../core/ui/Notification/Notification';
import Constant from  '../../../../core/utils/Constant';
import { checkInputExit} from '../../../../core/utils/validate';
import './styles.scss';

const { USER_ERROR, API_STATUS} = Constant;
const { onOpenNotification } = Notification;
@inject('rootStore')
@inject('apiStore')
@inject('menuRoleStore')
@observer
class Detail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            disabled: false,
            isInputNameShowError : false,
            isInputDescriptionShowError: false,
            isEdit : false,
            name: {
                value: '',
                isShowError: false,
                errorMsg: '',
            },
            phoneBox: {
                value: '',
                isShowError: false,
                errorMsg: '',
            },
            userRole:{
                value: '',
                isShowError: false,
                errorMsg: '',
            }
        }
    }
    
    componentDidMount (){
        this.props.rootStore.account.getAllRole();
    }

    hanldeCheckInputChange = () => {
        const { name, phoneBox, userRole } = this.state;

        if(checkInputExit(userRole.value)){
            this.setState({
                userRole:{
                    ...userRole,
                    isShowError: false,
                    errorMsg: USER_ERROR.EMPTY,
                },
                disabled: false,
            });
            this.props.rootStore.account.setName(name.value);
            this.props.rootStore.account.setPhone(phoneBox.value);
            this.props.rootStore.account.setRole(userRole.value);
        } else {
            this.setState({
                userRole:{
                    ...userRole,
                    isShowError: true,
                    errorMsg: USER_ERROR.REQUIRED,
                },
                disabled: true,
            })
        }
    }

    // Change detail in store
    handleInputName = (value) =>{
        this.setState({
            name:{
                ...this.state.name,
                value: value.target.value,
                isShowError: false,
                errorMsg: '',
            },
            disabled: false
        }, () => this.hanldeCheckInputChange());
    }        
    handleInputPhone = (value) =>{
        this.setState({
            phoneBox:{
                ...this.state.phoneBox,
                value: value.target.value,
                isShowError: false,
                errorMsg: '',
            },
            disabled: false
        }, () => this.hanldeCheckInputChange());
    }        

    handleSelectRole = (value) =>{
        this.setState({
            userRole:{
                ...this.state.userRole,
                value: value,
            }
        }, () => this.hanldeCheckInputChange());
    }

    handleEditCallBack = () => {
        const { isEdit } = this.state;
        const res = this.props.rootStore.account.response ? this.props.rootStore.account.response : '';
        let msgNoti = '';
        let editStatus = '';

        if(res && res.status && res.status.toUpperCase() === API_STATUS.SUCCESS){
            this.setState({
                isEdit: false,
                disabled: false
            })
            this.handleCancel();
            msgNoti = 'Edit Successful';
            editStatus = API_STATUS.SUCCESS;
        } else {
            this.setState({
                isEdit: false,
                disabled : false
            });
            msgNoti = 'Edit UnSuccessful';
            editStatus = API_STATUS.ERROR;
        }
        this.openNotification(msgNoti, editStatus);
    }

    handleSaveChange = () => {
        this.setState({
            disabled: true
        })
        this.props.rootStore.account.edit(this.handleEditCallBack);
    }

    handleClickFirst = () => {
        const { isEdit } = this.state;
        const accountDetail = this.props.rootStore.account.accountDetail;
        if (isEdit === false) {
            this.setState({
                name: {
                    ...this.state.name,
                    value: accountDetail.userName
                },
                phoneBox: {
                    ...this.state.phoneBox,
                    value: accountDetail.phoneNumber
                },
                userRole: {
                    ...this.state.phoneBox,
                    value: accountDetail.roleId
                }
            })
        };
        this.setState((prevSate) => ({
            isEdit: {
                ...prevSate.isEdit,
                isEdit : !prevSate.isEdit
            },
       
        }), () => {
            if(!isEdit) {
                this.setState({
                    disabled: true,
                })
            } else {
                this.handleSaveChange();
            }
        });
    }

    handleCancel =() =>{
        const { isEdit } = this.state;
        const status = this.props.rootStore.model.editModelStatus.status ? this.props.rootStore.model.editModelStatus.status.toUpperCase() : '';
        const hasChange = status === 'SUCCESS' ? true : false ;
        this.setState({
            isEdit: false,
            disabled: false,
        }, () => this.props.handleClickCancelCallback());
       
    }

    // Notification
    openNotification = (msg, status) => {
        onOpenNotification(msg, status);
    }

    render(){
        const { menuRoles } = this.props.menuRoleStore;
        const { rootStore} = this.props;
        const { isEdit, phoneBox, name, userRole } = this.state;
        const { accountDetail } = rootStore.account;
        const roles = toJS(rootStore.account.roles);
        const { loading } = this.props.apiStore;
        const data = [
            {
                "title"  : 'ID',
                "content": accountDetail.id ? accountDetail.id : '--',
                'hasFooter': true,
                "hasInput" : false
            },
            {
                "title"  : 'Name',
                "content": isEdit ? (<div className="modal-col-input-wapper">
                <VodaInput className="add-device-input" 
                    onChange={this.handleInputName} 
                    value={name.value}
                    placeholder={ accountDetail.userName ?  accountDetail.userName : 'Type'}
                    options={{
                        maxLength: 50,
                        isShowError: name.isShowError,
                        errorMsg: name.errorMsg,
                    }}
                />
             </div>) 
           : ( accountDetail.userName ? <span className="modal-col-input-content">{accountDetail.userName}</span> : '--'),
                'hasFooter': true,
                "hasInput" : isEdit
            },
            {
                "title"  : 'Email',
                "content": accountDetail.email ? <span className="modal-col-input-content" >{accountDetail.email}</span> : '--',
                'hasFooter': true,
                "hasInput" : false
            },
            {
                "title"  : 'Phone',
                "content": isEdit ? (<div className="modal-col-input-wapper">
                <VodaInput className="add-device-input" 
                    onChange={this.handleInputPhone} 
                    value={phoneBox.value}
                    placeholder={ accountDetail.phoneNumber ?  accountDetail.phoneNumber : 'Type'}
                    options={{
                        maxLength: 50,
                        isShowError: phoneBox.isShowError,
                        errorMsg: phoneBox.errorMsg,
                    }}
                />
             </div>) 
           : ( accountDetail.phoneNumber ? <span className="modal-col-input-content" >{accountDetail.phoneNumber}</span> : '--'),
                'hasFooter': true,
                "hasInput" : isEdit
            },
            {
                "title"  : 'Roles',
                "content": isEdit ? (<div className="modal-col-input-wapper">
                <SelectBox className={userRole.isShowError ? 'add-device-input-error' : "add-device-input"} 
                    onChange={this.handleSelectRole} 
                    value={userRole.value}
                    option={roles}
                    defaultValue={ accountDetail.role ? accountDetail.role : ''}
                    options={{
                        isShowError: userRole.isShowError,
                        errorMsg: userRole.errorMsg,
                    }}
                />
             </div>) 
           : ( accountDetail.role ? <span className="modal-col-input-content" >{accountDetail.role}</span> : '--'),
                'hasFooter': true,
                "hasInput" : isEdit
            }
        ];
        return(
            <div className="modal-model-detail-wapper">
                <InfoBox data={data}/>
                {
                    menuRoles && menuRoles.user == 'operation.write'? 
                    <ButtonFooter text={ isEdit ? "Save" : "Edit" } id={ isEdit ? "vfsm-button-footer-modal-save" : "vfsm-button-footer-modal-edit" }
                        className="model-detail-button"
                        disabled={this.state.disabled} 
                        loading={loading}
                        handleClickCancel={this.handleCancel} 
                        handleClickFirst={this.handleClickFirst}/>: null
                }
            </div>
        )
    }
}

Detail.propTypes = {
    id: PropTypes.string.isRequired,
}

export default Detail;

