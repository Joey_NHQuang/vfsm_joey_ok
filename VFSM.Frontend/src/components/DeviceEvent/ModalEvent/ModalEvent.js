
import React from 'react';
import { Row, Col, Input, Modal } from 'antd';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import VodaTable from '../../../common/VodaTable/VodaTable';
import IconSvg from '../../../common/icon/IconSvg';
import DeviceGoogleMap from '../../../common/GoogleMap/DeviceGoogleMap';
import VodaButton from '../../../common/VodaButton/VodaButton';
import SeriesHistory from './SeriesHistory';
import './styles.scss';

@inject('rootStore')
@observer
class ModalEvent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showMap: false,
            latDevice: null,
            lngDevice: null,
            latBeacon: null,
            lngBeacon: null,
            openSeries: false,
            activityId: undefined,
            deviceId: undefined
        }
    }

    showGoogleMap = (e) => {
        if (e.locationSource === 'DEVICE' || e.locationSource === 'LBS') {
            this.setState({
                showMap: true,
                latDevice: e.lat,
                lngDevice: e.lng,
                latBeacon: null,
                lngBeacon: null,
                activityId: e.activityId
            });
        } else {
            this.setState({
                showMap: true,
                latDevice: null,
                lngDevice: null,
                latBeacon: e.lat,
                lngBeacon: e.lng,
                activityId: e.activityId
            });
        }
    }

    // handleBackToTable = () => {
    //     this.setState({
    //         showMap: false
    //     });
    // }

    handleOpenSeriesHistory = (activityId, deviceId) => {
        this.setState({ openSeries: true, activityId: activityId, deviceId: deviceId });
    }

    handleCloseSeriesHistory = () => {
        this.setState({ openSeries: false, activityId: undefined, deviceId: undefined });
    }

    // Render History modal
    renderSeriesHistory = () => {

        const { openSeries, activityId, deviceId } = this.state;
        if (openSeries) {
            return (
                <Modal
                    width={1060}
                    footer={null}
                    title={'Series History [Device ID : ' + deviceId + ']'}
                    visible={openSeries}
                    onCancel={this.handleCloseSeriesHistory}
                >
                    <SeriesHistory activityId={activityId} />
                </Modal>
            )
        }
        return null;
    };
    
    handleCloseMap = () => {
        this.setState({
            showMap: false,
            latDevice: null,
            lngDevice: null,
            latBeacon: null,
            lngBeacon: null
        });
    }

    renderTable = () => {

        const { columns, data, haveSeries } = this.props;
        const columnNew = columns.slice();
        const dataNew = data.slice();

        if (haveSeries === true) {
            columnNew.push({
                title: 'Location',
                dataIndex: 'location',
                width: '15%'
            });
            columnNew.push({
                title: 'Actions',
                dataIndex: 'actions',
                width: '15%'
            });
            columnNew.push({
                title: 'Accuracy',
                dataIndex: 'accuracy',
                width: '10%'
            });
            dataNew.map((e, index) => {
                e.key = index;
                e.location = <VodaButton onClick={() => this.showGoogleMap(e)} text="See map" iconType="Location" ></VodaButton>;
                e.actions = <VodaButton onClick={() => this.handleOpenSeriesHistory(e.activityId, e.deviceId)} text="Series History" ></VodaButton>;
                return e;
            });
        } else {
            columnNew.push({
                title: 'Location',
                dataIndex: 'location',
                width: '15%'
            });
            columnNew.push({
                title: 'Accuracy',
                dataIndex: 'accuracy',
                width: '15%'
            });
            dataNew.map((e, index) => {
                e.key = index;
                e.location = <VodaButton onClick={() => this.showGoogleMap(e)} text="See map" iconType="Location" ></VodaButton>;
                return e;
            });
        }
        return <VodaTable columns={columnNew} data={dataNew} scroll={{ y: 460 }} />;

    }

    renderMap = () => {
        const { latDevice, lngDevice, latBeacon, lngBeacon, showMap, activityId } = this.state;
        const datalocation = [];
        if (latDevice !== null && latDevice !== undefined  && lngDevice !== null && lngDevice  !== undefined ) {
            datalocation.push(
                {
                    title: 'Device',
                    location: {
                        lat: latDevice ,
                        lng: lngDevice ,
                    }
                }
            )
        }
        if (latBeacon !== null && latBeacon !== undefined && lngBeacon !== null && lngBeacon !== undefined) {
            datalocation.push(
                {
                    title: 'Beacon',
                    location: {
                        lat: latBeacon,
                        lng: lngBeacon,
                    }
                }
            )
        }
        return datalocation.length > 0 
            ? 
            (
                <Modal
                    width={1200}
                    footer={null}
                    title={"Activity ID : " + activityId}
                    visible={showMap}
                    onCancel={this.handleCloseMap}
                    className="see-more-modal"
                >
                    <DeviceGoogleMap data={datalocation} />
                </Modal>
            )
            : (
                <Modal
                    width={1200}
                    footer={null}
                    title={"Activity ID : " + activityId}
                    visible={showMap}
                    onCancel={this.handleCloseMap}
                    className="see-more-modal"
                >
                    <div style={{ fontSize: '14px', fontWeight: 200, padding: '50px 210px', fontStyle: 'italic' }}>This device have not location</div>
                </Modal>
            )
    }

    render() {

        const { columns, data , typeModal} = this.props;

        return (
            <div className="modal-event" id={typeModal ? typeModal : ''}>
                {
                    data
                        ? this.renderTable()
                        : <div style={{ fontSize: '30px', fontWeight: 500, textAlign: 'center', padding: '50px', fontStyle: 'italic' }}>No found the result</div>
                }
                { this.state.showMap ? this.renderMap(): '' }
                {this.renderSeriesHistory()}
            </div>
        )
    }

}

ModalEvent.propTypes = {
}

export default ModalEvent;