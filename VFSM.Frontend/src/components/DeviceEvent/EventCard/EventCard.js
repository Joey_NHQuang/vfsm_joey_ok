import React, { Component } from 'react';
// import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { Avatar, Modal, Row, Col, Popover } from 'antd';
import SmartDevice from './images/SmartDevice.svg';
import { DateTime } from '../../../core/utils';
import ModalEvent from '../ModalEvent/ModalEvent';
import DeviceModal from '../../Monitoring/DeviceModal/DeviceModal';
import IconSvg from '../../../common/icon/IconSvg';
import IconFall from './images/FALL@2x.svg';
import IconSOS from './images/SOS@2x.svg';
import IconHistory from './images/History@2x.svg';

import HistoryModal from '../ModalEvent/HistoryModal';
import BatteryModal from '../ModalEvent/BatteryModal';
import FallModal from '../ModalEvent/FallModal';
import SOSModal from '../ModalEvent/SOSModal';

import './styles.scss';

const { formatTime } = DateTime;

class EventCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openDevice: false,
            openEventHistory: false,
            openEventSos: false,
            openEventFall: false,
            openBattery: false
        };
    }

    // Close Device modal
    handleCloseDeviceModal = () => {
        this.setState({ openDevice: false });
    };

    // Show device modal
    handleDeviceDetail = (deviceId) => {
        this.setState({ openDevice: true });
    }

    // Render modal view Device detail
    renderDeviceModal = () => {

        const { openDevice } = this.state;

        if (openDevice) {
            return (
                <Modal
                    width={693}
                    footer={null}
                    title="Device Detail"
                    visible={openDevice}
                    onCancel={this.handleCloseDeviceModal}
                >
                    <DeviceModal deviceId={this.props.device.deviceId} onClose={this.handleCloseDeviceModal} />
                </Modal >
            )
        }
        return null;
    };

    handleEventHistory = (deviceId) => {
        this.setState({ openEventHistory: true });
    }

    handleCloseHistoryModal = () => {
        this.setState({ openEventHistory: false });
    }

    handleEventSos = (deviceId) => {
        this.setState({ openEventSos: true });
    }

    handleCloseSOSModal = () => {
        this.setState({ openEventSos: false });
    }

    handleEventFall = (deviceId) => {
        this.setState({ openEventFall: true });
    }

    handleCloseFallModal = () => {
        this.setState({ openEventFall: false });
    }

    handleBattery = (deviceId) => {
        this.setState({ openBattery: true });
    }

    handleCloseBatteryModal = () => {
        this.setState({ openBattery: false });
    }

    // Render History modal
    renderHistoryModal = () => {

        const { openEventHistory } = this.state;
        if (openEventHistory) {
            return (
                <Modal
                    width={967}
                    footer={null}
                    title={'Event History [IMEI : ' + this.props.device.imei + ']'}
                    visible={openEventHistory}
                    onCancel={this.handleCloseHistoryModal}
                >
                    <HistoryModal deviceId={this.props.device.deviceId} />
                </Modal >
            )
        }
        return null;
    };

    // Render SOS modal
    renderSOSModal = () => {

        const { openEventSos } = this.state;
        if (openEventSos) {
            return (
                <Modal
                    width={967}
                    footer={null}
                    title={'Event SOS [IMEI : ' + this.props.device.imei + ']'}
                    visible={openEventSos}
                    onCancel={this.handleCloseSOSModal}
                >
                    <SOSModal deviceId={this.props.device.deviceId} />
                </Modal >
            )
        }
        return null;
    };

    // Render Fall modal
    renderFallModal = () => {

        const { openEventFall } = this.state;
        if (openEventFall) {
            return (
                <Modal
                    width={967}
                    footer={null}
                    title={'Event FALL [IMEI : ' + this.props.device.imei + ']'}
                    visible={openEventFall}
                    onCancel={this.handleCloseFallModal}
                >
                    <FallModal deviceId={this.props.device.deviceId} />
                </Modal >
            )
        }
        return null;
    };

    // Render Fall modal
    renderBatteryModal = () => {

        const { openBattery } = this.state;
        if (openBattery) {
            return (
                <Modal
                    width={967}
                    footer={null}
                    title={'Battery History [IMEI : ' + this.props.device.imei + ']'}
                    visible={openBattery}
                    onCancel={this.handleCloseBatteryModal}
                >
                    <BatteryModal deviceId={this.props.device.deviceId} />
                </Modal >
            )
        }
        return null;
    };

    render() {

        const { device } = this.props;

        return (
            <div className="event-card">
                <Row gutter={8}>
                    <Col className="gutter-row" span={5}>
                        <span className="event-device-time">{formatTime(device.activatedDate)}</span>
                    </Col>
                    <Col className="gutter-row" span={9}>
                        <div className="event-device-icon" onClick={() => this.handleDeviceDetail(device.deviceId)}><Avatar shape="square" src={SmartDevice} /></div>
                        <Popover placement="bottom" content={device.imei} trigger="hover">
                            <div className="event-device-name">
                                <span>{device.imei}</span>
                            </div>
                        </Popover>
                    </Col>
                    <Col className="gutter-row" span={3}>
                        {
                            device.lastActivityId && <div className="event-other-icon" onClick={() => this.handleEventHistory(device.deviceId)}>
                                <Avatar shape="circle" size={50} src={IconHistory} />
                            </div>
                        }
                    </Col>
                    <Col className="gutter-row" span={3}>
                        {
                            device.haveSos && <div className="event-other-icon" onClick={() => this.handleEventSos(device.deviceId)}>
                                <Avatar shape="circle" size={50} src={IconSOS} />
                            </div>
                        }
                    </Col>
                    <Col className="gutter-row" span={3}>
                        {
                            device.haveFall && <div className="event-other-icon" onClick={() => this.handleEventFall(device.deviceId)}>
                                <Avatar shape="circle" size={50} src={IconFall} />
                            </div>
                        }
                    </Col>
                </Row>
                <div className="event-battery-icon" onClick={() => this.handleBattery(device.deviceId)}>
                    {
                        device.battery >= 75 && device.battery <= 100
                            ? <IconSvg name="Pin4" />
                            : device.battery >= 50 && device.battery < 75
                                ? <IconSvg name="Pin3" />
                                : device.battery >= 25 && device.battery < 50
                                    ? <IconSvg name="Pin2" />
                                    : device.battery > 0 && device.battery < 25
                                        ? <IconSvg name="Pin1" />
                                        : device.battery == 0
                                            ? <IconSvg name="Pin0" />
                                            : <div></div>
                    }
                </div>

                {this.renderDeviceModal()}
                {this.renderHistoryModal()}
                {this.renderFallModal()}
                {this.renderSOSModal()}
                {this.renderBatteryModal()}
            </div>

        );
    }

};

EventCard.propTypes = {
    device: PropTypes.object.isRequired
}

export default EventCard;
