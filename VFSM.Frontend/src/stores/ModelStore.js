import { observable, configure, action, runInAction } from 'mobx';
import BaseStore from '../core/stores/BaseStore';
import { fromJS } from 'immutable';
import { ModelService } from '../services'


// don't allow state modifications outside actions
configure({ enforceActions: 'observed' });

export default class ModelStore extends BaseStore {
    @observable models = [];
    @observable details = {};
    @observable addModelStatus = {};
    @observable editModelStatus = {};

    // get all model
    @action
    async getAll(sortType, page) {
        const jobRequest =  () => { return  ModelService.getAll(sortType, page); };
		this.call(jobRequest,
			response => {
                this.models = response.response;
            }   
		);
    }

    // get detail
    @action
    async getDetail(idModel) {
        const jobRequest = () => {return ModelService.getDetail(idModel);}
        this.call(jobRequest,
            response =>{
                this.details = response.response;
            }
        );
    }
    
    // add new model
    @action
    async addNewModel(body, callBack) {
        const jobRequest = () => {return ModelService.addNewModel(body)}
        this.call(jobRequest,
            response => {
               if(response.response.status.toUpperCase() === 'SUCCESS'){
                    this.models.items.unshift(response.response.data);
               }
               this.addModelStatus = response.response;
               if(callBack) {
                 callBack();
               }
            },
        );

    }

    // edit model
    async editModel(body, callBack) {
        const jobRequest = () => {return ModelService.editModel(body)}
        this.call(jobRequest,
            response => {
                if(response.response.status.toUpperCase() === 'SUCCESS'){
                    const index = this.models.items.findIndex(item => item.modelId === body.modelId);
                    this.models.items[index] = response.response.data;
                    this.details = response.response.data;
                }
                this.editModelStatus = response.response;
                if(callBack) {
                  callBack();
                }
             },
        );
    }

    // delete model
    @action
    async deleteModel(idModel, callBack) {
        const jobRequest = () => { return ModelService.deleteModel(idModel)}
        this.call(jobRequest, 
            response => {
                if(response.response.status.toUpperCase() === 'SUCCESS'){
                    const newModel = this.models.items
                    newModel.splice(newModel.findIndex(item => item.modelId === response.response.data.modelId), 1);
                    this.details = response.response;
                    this.models.items = [...newModel];
                }
                if(callBack){
                    callBack();
                }
            }
        );
    }

}